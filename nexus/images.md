# Nexus Images

### Overview of Expanded City - comparison between vanilla area and expanded area

![](/images/overview-vanillacompare.png)

### Overview of Expanded City - restored wallwalks forming a new road network

![](/images/overview-wallwalknetwork.png)

### Overview of Expanded City - three new districts, restored arena structure, expanded market, new areas

![](/images/overview-districtsandareas.png)

### Overview of Expanded City - various structures and buildings full of potential

![](/images/overview-structuresanddoors.png)

### Western District Overview

![](/images/overview-westerndistrict.png)

### Northern District Overview

![](/images/overview-northerndistrict.png)

### Northeast District Overview

![](/images/overview-northeastdistrict.png)

### Southern wallwalk, looking west

![](/images/southernwallwalk1.png)

### Southern wallwalk at eye level, looking towards Western Fort

![](/images/southernwallwalk6.png)

### Southern Wallwalk, looking east

![](/images/southernwallwalk2.png)

### Southern Wallwalk from across the river

![](/images/southernwallwalk8.png)

### Cats walking by the city

![](/images/southernwallwalk17.png)

### Underneath the coverings of Southern Wallwalk
![](/images/southernwallwalk9.png)

### Underneath the coverings in the evening
![](/images/southernwallwalk13.png)

### Southern Wallwalk catapult bastions
![](/images/southernwallwalk10.png)

### Southern Wallwalk catapult bastions
![](/images/southernwallwalk15.png)

### High Road view of Southern Wallwalk battlements and the South Wall Tower
![](/images/southernwallwalk11.png)

### Western Fort direct front view

![](/images/westernfort1.png)

### Western Fort bird's eye view

![](/images/westernfort2.png)

### Western Fort looking eastward

![](/images/westernfort3.png)

### View from Western Fort, looking east

![](/images/westernfort4.png)

### Expanded market looking south, with reorganized stands and stairways up to wall level

![](/images/expandedmarket1.png)

### Expanded market looking north

![](/images/expandedmarket2.png)

### New attic door to Blacksmith Quarters at wall level

![](/images/blacksmithattic1.png)

### View from Blacksmith Quarters up to the walls
![](/images/southernwallwalk12.png)

### Market extends into Western District underneath High Road bridge

![](/images/expandedmarket3.png)

### Meadhall-Bakery building in the new Expanded Market

![](/images/expandedmarket4.png)

### High Road (used to be vanilla's western wall)

![](/images/highroad1.png)

### Staircases connecting Valunstrad to the High Road

![](/images/highroad3.png)

### High Road in the evening
![](/images/highroad4.png)

### Meadhall

![](/images/meadhall1.png)

### Meadhall in the evening
![](/images/meadhall2.png)

### Meadhall from wallwalk at night
![](/images/meadhall3.png)

### Meadhall Apartments (backside of Meadhall)

![](/images/meadhallapartments1.png)

### View into Western District road-alleys

![](/images/westerndistrictalleys4.png)

### Western District road-alleys

![](/images/westerndistrictalleys1.png)

### Western District road-alleys

![](/images/westerndistrictalleys2.png)

### Western District road-alleys

![](/images/westerndistrictalleys3.png)

### Windhelm Bathhouse

![](/images/bathhouse1.png)

### Western District Temple towers above the alleys

![](/images/westerndistricttemple1.png)

### Western District Temple skyway connection to Mountain Caves

![](/images/westerndistricttemple2.png)

### Windswept Manor

![](/images/windsweptmanor1.png)

### Windswept Manor from above, showcasing the balcony

![](/images/windsweptmanor2.png)

### View of Windswept Manor from city

![](/images/windsweptmanor3.png)

### Windswept Manor Commons

![](/images/windsweptmanorinterior1.png)

### View from Windswept Manor balcony - wallwalks at night
![](/images/southernwallwalk14.png)

### View from Windswept Manor balcony - morning sun
![](/images/windsweptmanor4.png)

### Stairs up to Beacon of Windhelm

![](/images/beacon1.png)

### Beacon of Windhelm

![](/images/beacon2.png)

### Gate into the mountain

![](/images/mountaingate1.png)

### Northern District behind Valunstrad

![](/images/northerndistrict1.png)

### View of Valunstrad from plaza

![](/images/northerndistrict3.png)

### Bank of Windhelm

![](/images/bank1.png)

### Northern District Temple

![](/images/northerndistricttemple1.png)

### Northern District Temple from ground level

![](/images/northerndistricttemple2.png)

### Northern District Temple from Northern Wallwalk

![](/images/northerndistricttemple3.png)

### Stonemasonry Guild

![](/images/stonemasonryguild1.png)

### Northern District Residences

![](/images/northerndistrictresidences2.png)

### Northern District Mansion

![](/images/northerndistrictmansion1.png)

### Mountain Gate in the evening - with Beacon in background
![](/images/mountaingate2.png)

### High Road evening view towards south
![](/images/highroad5.png)

### Northern District Mansion at night
![](/images/northerndistrictmansion2.png)

### Northern District stairs down to Palace of Kings

![](/images/northerndistrict4.png)

### Windhelm Arena structural restoration

![](/images/arena1.png)

### Windhelm Arena entrance

![](/images/arena2.png)

### Windhelm Arena entrance area

![](/images/arena3.png)

### Windhelm Arena Market (long, elevated, covered wallwalk-level market)

![](/images/arenamarket1.png)

### View of Talos statue from Arena Market

![](/images/arenamarket2.png)

### Gray Quarter below Arena Market

![](/images/arenamarket3.png)

### Underneath Arena Market roofs - view to the west

![](/images/arenamarket4.png)

### Arena Market entrance in the evening
![](/images/arenamarket11.png)

### Underneath Arena Market roofs - night view towards Dockside East Tower
![](/images/arenamarket12.png)

### Gray Quarter at night with the Arena Market's presence above
![](/images/arenamarket10.png)

### Underneath Arena Market roofs - view to the east

![](/images/arenamarket5.png)

### View of Northeast District Castle from Arena Market

![](/images/arenamarket6.png)

### Arena Market - stairs down to lower area

![](/images/arenamarket7.png)

### Lower area of Arena Market
![](/images/arenamarket9.png)

### Lower area of Arena Market and the Northeast District Temple
![](/images/arenamarket8.png)

### Northeast District alley view towards Eastern Tower
![](/images/easterntower1.png)

### Northeast District Residences and Pub
![](/images/pub1.png)

### Northeast Gatehouse
![](/images/northeastgatehouse1.png)

### Northeast District Residences
![](/images/northeastdistrictresidences1.png)

### Northeast District Castle view from gatehouse
![](/images/northeastdistrictcastle1.png)

### Northeast District Castle - closer view
![](/images/northeastdistrictcastle2.png)

### Northeast District Castle view from Northeast Wallwalk
![](/images/northeastdistrictcastle3.png)

### Northeast District Castle - further view
![](/images/northeastdistrictcastle4.png)

### Road to Northeast District Castle from Arena Market
![](/images/northeastdistrictcastle8.png)

### Arena Market lower area at night
![](/images/arenamarket13.png)

### Eastern Tower at night
![](/images/easterntower3.png)

### Northeast Gate view from outside
![](/images/northeastgate1.png)

### Northeast Gate direct front view
![](/images/northeastgate2.png)

### Northeast Gate and Castle, further up road from Winterhold (road IP)
![](/images/northeastgate3.png)

### Northeast Gate and Castle in snow
![](/images/northeastgate5.png)

### Wolves playing outside Northeast Gate and Castle
![](/images/northeastgate4.png)

### Northern Wallwalk extends out from Northeast Castle
![](/images/northernwallwalk1.png)

### Northern Wallwalk extending behind Palace of the Kings
![](/images/northernwallwalk2.png)

### Northern Wallwalk view towards Northeast Castle
![](/images/northernwallwalk3.png)

## Morning view from Northern Wallwalk - east side of city
![](/images/northernwallwalk4.png)

## Morning view from Northern Wallwalk - west side of city
![](/images/northernwallwalk5.png)

### Northeast District Castle railing
![](/images/northeastdistrictcastle5.png)

### View from Northeast District Castle railing
![](/images/northeastdistrictcastle6.png)

### Northeast District Castle view towards back plaza
![](/images/backplaza1.png)

### Back Plaza
![](/images/backplaza2.png)

### Northeast District Castle Side Room
![](/images/northeastdistrictcastle7.png)

### Northeast District - view from descending stairs
![](/images/northeastdistrict1.png)

### Northeast District courtyard
![](/images/northeastdistrict2.png)

### Northeast District courtyard from another angle
![](/images/northeastdistrict3.png)

### Northeast District Warehouse
![](/images/northeastdistrictwarehouse1.png)

### Eastern Tower bird's eye
![](/images/easterntower2.png)

### Northeast District bird's eye
![](/images/northeastdistrict4.png)

### Gray Quarter building fixes (there used to be jarring holes when looked from top)
![](/images/grayquarterfixes1.png)

### Gray Quarter Wallwalk and Storerooms, and Dockside East Tower
![](/images/grayquarterwallwalk1.png)

### Eastern Tower, Gray Quarter Wallwalk and Dockside East Tower from outside the walls
![](/images/grayquarterwallwalk2.png)

### Dockside Keep on the other side of Gray Quarter
![](/images/docksidekeep1.png)

### View of Dockside East Tower from Dockside Area
![](/images/dockside2.png)

### Dockside Keep
![](/images/docksidekeep2.png)

### Dockside Keep
![](/images/docksidekeep3.png)

### Dockside Keep at night
![](/images/docksidekeep5.png)

### Stairs linking Aretino residence road with Dockside area
![](/images/docksidekeep4.png)

### Dockside Keep bird's eye
![](/images/dockside1.png)

### Dockside treadmill cranes
![](/images/dockside3.png)

### Dockside East Tower and surrounding area from outside the city
![](/images/docksideeasttower1.png)

### Docks view from outside the city
![](/images/docks2.png)

### Docks view from outside the city
![](/images/docks1.png)

### Docks view from the south
![](/images/docks3.png)

### Dockside South Tower
![](/images/docksidesouthtower1.png)

### Dockside South Tower rising behind Niranye's House
![](/images/docksidesouthtower2.png)

### Beneath Dockside South Tower's stairs
![](/images/docksidesouthtower3.png)

### South Gatehouse
![](/images/southgatehouse1.png)

### South Gatehouse up close
![](/images/southgatehouse5.png)

### South Gatehouse in a blizzard
![](/images/southgatehouse3.png)

### South Gatehouse view from outside
![](/images/southgatehouse2.png)

### Guards standing in front of the south gate
![](/images/southgatehouse4.png)

### Guard of Windhelm
![](/images/southernwallwalk3.png)

### Southern Wallwalk from road across the river
![](/images/southernwallwalk4.png)

### Southern Wallwalk from road across the river
![](/images/southernwallwalk5.png)

### South Gatehouse at night
![](/images/southgatehouse6.png)

### Dockside South Tower door at night
![](/images/docksidesouthtower4.png)

### The entire southern wallwalk stretch (with everything) at night
![](/images/southernwallwalk16.png)