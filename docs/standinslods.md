- [Standins and LODs](#standins-and-lods)
  - [What Are Standins and LODs?](#what-are-standins-and-lods)
    - [Standins](#standins)
    - [LODs](#lods)
  - [How Are Standins and LODs Handled?](#how-are-standins-and-lods-handled)
    - [Standins](#standins-1)
    - [LODs](#lods-1)
    - [Workflow](#workflow)
  - [What does the `ewefinalizer` xEdit script do, exactly?](#what-does-the-ewefinalizer-xedit-script-do-exactly)
    - [During Initialize](#during-initialize)
    - [As xEdit Processes Each Record](#as-xedit-processes-each-record)
    - [During Finalize](#during-finalize)
      - [Process Standins](#process-standins)
      - [Process LODs](#process-lods)
    - [Additional Notes](#additional-notes)
  - [Other Discussions](#other-discussions)
    - ["Is Full LOD" flags vs actual LODs between 0.2.0 and 0.3.0](#is-full-lod-flags-vs-actual-lods-between-020-and-030)
    - [LOD Model Quality](#lod-model-quality)
    - [Performance Observations](#performance-observations)

# Standins and LODs

This document serves as technical info on how standins and LODs are handled in this mod currently. It may be of interest to folks who find standin/LOD-related bug, possibly with respect to DynDOLOD. It may also be of interest to folks who want to fork this mod.

Note that I am not an expert in how LODs work. I have only read the DynDOLOD documentation as a user/mod author, and I read it more as a resource for problem solving and less as academic literature, so take what I say/what I did with a grain of salt, and be mindful of any mistakes/incorrect assumptions I might have made.

What I've done seem to have worked, seem to be maintainable, and all make sense in my head. That said, folks who know better please feel free to give further advice on how to optimize the standin/LOD solution for this mod. I would always appreciate guidance.

## What Are Standins and LODs?

### Standins

**Standin** is a term I'm using to describe copies of objects made across parent and child worldspaces in order to keep the view consistent within the loaded cells. In EWE's problem space, we have *Tamriel Worldspace Standins* and *Windhelm Worldspace Standins*.

**Tamriel Worldspace Standins** are Tamriel worldspace copies of objects added by EWE in the Windhelm worldspace. In EWE, when you approach Windhelm from the outside, you will see Windhelm's newly expanded city footprint not only in the distance (via LODs), but also up close. The "up close" part is because we have these Tamriel worldspace standins. They serve the crucial role of making sure the inside is 99% the same as the outside.

**Windhelm Worldspace Standins** are Windhelm worldspace copies of vanilla game objects from the Tamriel worldspace. In EWE, when you are inside the city but look out from a high vantage point, you will see not only distant scenery (via LODs), but also the landscapes, waterfalls, cliffs, and buildings outside the city such as the Windhelm Bridge, the Windhelm Stables, or nearby farmhouses - even when up close. This is because we have these Windhelm worldspace standins. They are how the [Repairs to Vanilla Windhelm Worldspace](overview.md#repairs-to-vanilla-windhelm-worldspace) are made.

Since there are many objects that need standins, hand-creating them is not a feasible way to do things. Instead, it is much better to generate them with a script.

### LODs

**LOD** stand for "Level of Detail", and is generally referred to the way the game engine efficiently renders a mockup of distant scenery beyond the currently loaded cells.

From what I can understand, the way the game handles LODs is that they are **pre-baked** data. When mods add and remove objects, swap models, override textures, etc... the LODs **will not reflect this** until the user has regenerated them. The regeneration is typically done by hand via tools like ***xLODGen***, ***TexGen***, and ***DynDOLOD***.

The responsibility for regenerating LODs ***lies on the users***, because each modded game is unique, so LODs need to be specifically generated to tailor to your specific set of mod assets and load order. Here, EWE's responsibility is to ship with the correct files and configs for DynDOLOD to work correctly.

Since there are many custom objects that may need LODs, hand-managing each one by hand is not a feasible way to do things. Instead, it is much better to ensure LOD asset files exist with a script.

## How Are Standins and LODs Handled?

### Standins

Standins are copied between worldspaces for objects that need them.

### LODs

The mod ships with a DynDOLOD settings file at `DynDOLOD\DynDOLOD_epicwindhelmbaseesp.ini`. This file contain the following setting:

```
[Skyrim Settings]
IgnoreWorlds=WindhelmWorld
```

The above setting tells DynDOLOD to ignore all Windhelm child worldspace objects when generating LODs. We need to do this because EWE adds many objects that DynDOLOD will try to add as an LOD by default, that should not be LODs because their addition are not worth their performance cost. This is the same strategy as in DynDOLOD's shipped configs for, say, JK's Skyrim.

The expanded Windhelm will nevertheless make it into the LOD because the Tamriel worldspace standins will result in LODs. The standins should only consist of major structures that make up the city's general look. Therefore it is much more lightweight, and a fitting source of data for LOD.

Any EWE-shipped custom object that need an LOD will need an LOD model shipped in the mod as well. Currently, the LOD models are simply copies of the full model.

### Workflow

The following is the current workflow I am using for updating standins and LOD data in the mod. Note that here, "LODs" are _**not**_ the end result LODs in the user's game. Instead, they are config files and assets data that need to be in the mod in order for the LODs to show up sufficiently well when the user generates them in their game.

First, the modder (i.e. me) would load up an extra plugin called `EpicWindhelm-LODStandinLabels.esp`. This plugin has `EpicWindhelmBase.esp` as a master, and its only role is to allow the modder to hand-label which objects need LODs and which objects need plugins. That's it.

To make a label, the modder would double-click on an object, and set its Editor ID to a string that looks like `ltrSL_XXXXXXXX`.

* The label string should start with `ltr`.
* The label string is then followed by `S`, `L`, or `SL`/`LS`. These are referred to as "modes" and basically denote to whether this object should have `S`tandin, `L`OD, or both.
* The label string is then followed by a `_` character, and then followed by the load order FormID of the object. The purpose of the load order FormID is to keep the label string unique, since the CK requires all EditorIDs to be unique.
  * In the CK window where Editor ID is modified, you can see the load order FormID right there, so it is easy to just type it in. The FormID is a hex string. `abcdef` are easily reachable on the left hand, while `0123456789` are easily reachable on the numpad with the right hand. Overall, hand-typing the FormID that you can see right there is a quick and easy thing to do.

Each session would start with the modder hand-updating these labels in the CK. If an object that used to need standins/LODs no longer do, the modder can update or remove the label. Newly added object can be newly labeled by the modder. Then, after a round of label updates is done, the modder would save the `EpicWindhelm-LODStandinLabels.esp` with updated changes.

Next, the modder would open up xEdit and load up _**exactly**_ `EpicWindhelmBase.esp` and `EpicWindhelm-LODStandinLabels.esp`. The modder would then:

1. Shift-click to select **both** and **only** these two plugins
2. Right click and select "Apply Script..." in the context menu
3. Find the `ewefinalizer` script, and run it.

See Demonstration Pics: [1](images/xedit/ewefinalizer1.png), [2](images/xedit/ewefinalizer2.png), [3](images/xedit/ewefinalizer3.png)

The `ewefinalizer` script would then does all the work of processing and updating the Standin/LOD data in the mod. Afterwards, the modder would run the usual `ITM`/`UDR` cleaning on both plugins, before saving them.

It is recommended that before each script run, the modder should back up the previous version of both plugins, so that if the script breaks things, the modder can revert to previous versions.

> The `ewefinalizer` script, as well as the current `EpicWindhelm-LODStandinLabels.esp` plugin, will be packaged with the mod release under a `dev` subfolder. To use them, the user would copy the `EpicWindhelm-LODStandinLabels.esp` into the top-level folder so that it may get picked up by the load order, and copy the `ewefinalizer.pas` script file into the `Edit Scripts` subfolder of their xEdit installation, so that it will show up there.

## What does the `ewefinalizer` xEdit script do, exactly?

The best way to know exactly what the script do is to read it and find out. The following is a high level description of its workflow.

### During Initialize

* Validates your load order to ensure it is the expected one.
* Queries `EpicWindhelmBase.esp` and `EpicWindhelm-LODStandinLabels.esp` for some information and remembers them for later work.

### As xEdit Processes Each Record

* If it is a label record (i.e. a record from `EpicWindhelm-LODStandinLabels.esp` containing a valid label string), parse and save the information for later work.
* If it is a standin record (i.e. a record from `EpicWindhelmBase.esp` containing an Editor ID string that matches the naming convention the script uses for standins), remember this information for later work.

### During Finalize

#### Process Standins

* Figure out what standins need to be added, updated, or deleted, depending on what existing standins there are, and what existing labels there are for standins.
* For standins that need to be added:
  * Create a copy of the original record as the standin record
* For standins that need to be added or updated:
  * Take the standin record and ensure its data is updated thus:
    * The standin should be located at the correct cell (based on position) in the worldspace opposite of the original record worldspace (i.e. Tamriel -> Windhelm, Windhelm -> Tamriel)
    * The standin should have the same base object as the original record
    * The standin should have the same scale, position, and rotation data as the original record
    * The standin should have EditorID with the naming scheme of `ltr_XXXXXXXX_standin`, where `XXXXXXXX` is the load order FormID of the original record.
  * Also update the label record's override data to match with the original record, to ensure things always look up-to-date with respect to the base mod when labeling in the CK.
* For standins that need to be deleted:
  * Remove the standin record

#### Process LODs

* For records that need LODs:
  * If the base record is a `STAT` object (which should be most if not all of them), read its `Model\MODL` path to get the `.nif` file location.
    * Ignore if the `.nif` is under a list of known vanilla paths these objects touch, including `meshes\architecture\windhelm`, `meshes\architecture\skyhaventemple`, or `meshes\landscape`.
    * Otherwise, we assume the model is a mod-added model. We would then ensure that a copy of it with the `_lod.nif` suffix exists in an `lods` subfolder under the parent folder of the model file. If the copy doesn't exist, make a copy there. This way, DynDOLOD can find the geometry to use for new custom models when generating LODs.

### Additional Notes

* The `ewefinalizer` script has `debug` boolean variable that can be set to `true` or `false` to toggle on/off debug output. The script should be faster to run if `debug` is off, but easier to see exactly what happened if `debug` is on.

* Standins are never recreated. Old standins that are already in `EpicWindhelmBase.esp` will be updated. Old standins will only disappear if there are no longer a label for it. This means unless the mod author (i.e. me) haphazardly unlabels and relabels things, `EpicWindhelmBase.esp` should remain fairly stable with respect to patches that depend on generated standin records.

## Other Discussions

### "Is Full LOD" flags vs actual LODs between 0.2.0 and 0.3.0

In version 0.2.0 of EWE, "LODs" are handled by setting the ***Is Full LOD*** flags on a lot of objects. In version 0.3.0 of EWE, those flags are reverted and LODs are expected to be generated via DynDOLOD.

The reason for this change is that, DynDOLOD is first and foremost the community best practice for the LOD problem. If nothing else, it is good to follow best and established practices.

Legit generated LODs should also have a performance improvement over the ***Is Full LOD*** hack, even if the LOD model is the same as the full model. One can imagine that the LOD baking process would produce better optimized data from the original geometry that are more lightweight to load than entire full model objects.

On the other side, the ***Is Full LOD*** flag has the severe downside that every model with this flag end up becoming a persistent object, which will get baked into the save game, and interfere with save game compatibility with respect to future updates to the mod.

Therefore, 0.3.0 now expects users to generate LODs via DynDOLOD. Pretty much all existing city expansion mods work this way anyway, so users should expect this. If DynDOLOD's dynamic portions are too heavy, the user can choose to generate LODs with DynDOLOD and then run without the plugins, which seem to have no noticeable performance differences from EWE base without any LOD generation.

Note that I did try to see if I can continue to support the ***Is Full LOD*** approach as an optional addon plugin for those that wants it. However, [in testing it seems that this flag won't work if introduced by an override](images/xedit/isfulllodproblem.png). It would only work if set on the original record. Thus I had to entirely abandon that idea.

### LOD Model Quality

Typically LOD models are lower poly than regular models. However since I lack 3D asset editing skills, creating optimized low-poly LOD models are beyond my ability. Currently I am not seeing clear evidence that LOD model quality is a performance bottleneck, so I am not prioritizing this.

### Performance Observations

The following observations are made with my modded Skyrim build I use for screenshots. This is a fairly intense build that pushes my machine's CPU/GPU/memory to more or less max.

With this build, when DynDOLOD is completely disabled, I get an average case FPS of ~45-50 in the vast majority of places, and a worst case of ~30 in a few places. During worst case, I can clearly see that the lower FPS is due to EWE-added objects, resulting in high draw calls. I can see this because these tend to be camera views where ENB-reported draw calls are at their highest, and these are also views where if I look up at the sky, FPS immediately gets back to 60 once the objects are no longer in camera.

If you want to see for yourself, the worst case views are mostly when you are at the top ledge of the [Gray Quarter Wallwalk](locations/grayquarterwallwalk.md) and looking in the direction of the [Windswept Manor](locations/windsweptmanor.md).

This "~45-50 average case, ~30 worst case", therefore, serve as a good baseline to gauge DynDOLOD's performance impact.

Given the above, when I generate DynDOLOD at low or medium settings and completely enable everything, my average case FPS is now about 30-45. I can tell this is lower FPS due to DynDOLOD because first, DynDOLOD is the only thing that changed, and second, when I previously look at the sky, FPS goes back up to 60, whereas now, when I look at the sky, FPS does not improve.

That said, it seems that the richer features of the DynDOLOD plugin makes up for the majority of the performance impact. When I generate DynDOLOD but disable `DynDOLOD.esm` and `DynDOLOD.esp`, I end up getting better performance similar to the baseline, despite still having baked LODs for the expanded Windhelm.

Overall, it looks like in the worst case scenario, you can choose to generate LODs but disable DynDOLOD's plugins to get negligible performance impact from DynDOLOD and still have EWE LODs. Do note that my screenshot build is pushing my computer to the limits. If you use less impactful graphic mods or have a beefier machine, you probably will not have my performance limitations.

Beyond that, if you are a power user of DynDOLOD, I imagine you can probably find plenty of ways to further optimize. If there are any additional settings that you know should be bundled into EWE releases, please contact me and let me know.

Nevertheless, I will likely continue to try to optimize DynDOLOD settings in future work on this mod, though I'm not an expert at it, and it may not be top priority. Right now, the only idea I can think of is reduce the number of standins that end up making it into the Tamriel worldspace. I will probably do a cleanup of that at some point.
