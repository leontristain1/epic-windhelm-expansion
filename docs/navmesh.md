- [Vanilla Navmesh Changes in EWE](#vanilla-navmesh-changes-in-ewe)
  - [Navmesh Changes](#navmesh-changes)
    - [Windhelm Worldspace](#windhelm-worldspace)
      - [Blacksmith Quarters Navmesh](#blacksmith-quarters-navmesh)
      - [Palace of the Kings Courtyard West-Side Navmesh](#palace-of-the-kings-courtyard-west-side-navmesh)
      - [Palace of the Kings Courtyard East-Side Navmesh](#palace-of-the-kings-courtyard-east-side-navmesh)
      - [Aretino-Prison Navmesh](#aretino-prison-navmesh)
      - [Market Square Navmesh](#market-square-navmesh)
      - [Clan Shatter-Shield Navmesh](#clan-shatter-shield-navmesh)
      - [Hjerim Navmesh](#hjerim-navmesh)
      - [Navmesh Sunk Below Ground](#navmesh-sunk-below-ground)
    - [Tamriel Worldspace](#tamriel-worldspace)
      - [Northeast Gate Navmesh](#northeast-gate-navmesh)
    - [Windhelm Pit Worldspace](#windhelm-pit-worldspace)
      - [Windhelm Pit Navmesh](#windhelm-pit-navmesh)

# Vanilla Navmesh Changes in EWE

This document serves as technical info for folks who might want to patch the mod with other mods, or generally trying to debug problems in their own load orders.

## Navmesh Changes

In EWE, the following vanilla navmesh were modified/extended. Other than what's mentioned here, there should be no other vanilla navmesh that are touched by this mod. This means other vanilla navmesh will not have override records in the mod's plugin.

-----

### Windhelm Worldspace

#### Blacksmith Quarters Navmesh
```
Cell: (32, 8) "WindhelmCandlehearthHallExterior"
NAVM Record: 0004b66c
```
* This navmesh covered the main road from the Southern Gate to the Market Square; it was extended up the newly added wall-side stairs to cover portions of Southern Wallwalk.

#### Palace of the Kings Courtyard West-Side Navmesh
```
Cell: (32, 10) "WindhelmPalaceOfTheKingsExterior"
NAVM Record: 000ed015
```
* This navmesh extended over to valunstrad to connect to a valunstrad navmesh; the valunstrad "stub" has been extended upward to climb the newly added stairs, and goes on to cover portions of the Northern District.

#### Palace of the Kings Courtyard East-Side Navmesh
```
Cell: (33, 10) "WindhelmPitExterior"
NAVM Record: 00098204
```
* This navmesh used to be a small corner stub for its cell within the Palace of the Kings Courtyard, and covered the wall directly opposite of the courtyard's west-side archway that led to Valunstrad. EWE introduces a symmetric archway on the east-side that leads to the space in front of the restored Arena, and from there connect to the Arena Market, Arena Prison Tower, and the stairs down to Northeast District. Thus, this navmesh has been extended to cover the remaining parts of the expanded area in this cell.

#### Aretino-Prison Navmesh
```
Cell: (33, 9) "WindhelmOrigin"
NAVM Record: 000f1a86
```
* This navmesh covered the road that goes through the Aretino Residence and the Prison structure. On the southern side, it has been extended up the Dockside Keep stairs and then over across the top of the Dock Gate to cover portions of the Gray Quarters Wallwalk. On the northern side, it has been extended through the cross-path of the new Arena Prison structure to cover portions of the Gray Quarters Wallwalk.

#### Market Square Navmesh
```
Cell: (31, 8) "WindhelmMarketplaceExterior"
NAVM Record: 000f7797
```
* This navmesh covered most of the Market Square; it has been extended further west to cover the expanded market square, then under the high road bridge to cover portions of the Western District. It has also been extended above the wallwalk to cover portions of the Southern Wallwalk and High Road. The vanilla "holes" in the navmesh that used to accomodate some vanilla positions of market stands have been reduced in size in response to those market stands being repositioned elsewhere in the mod.

#### Clan Shatter-Shield Navmesh
```
Cell: (31, 9)
NAVM Record: 0002e50f
```
* This navmesh covered the front of the Clan Shatter-Shield house. It has been extended up the newly added stairs to the left side of the house, to cover portions of the High Road. Along the High Road, it now goes north and then back down around to connect to the Hjerim navmesh.

#### Hjerim Navmesh
```
Cell: (31, 10)
NAVM Record: 000be45a
```
* This navmesh covered the front side of Hjerim. Its geometry was not changed by the mod, but some "stub" navmesh that it used to connect to (000ed002, 000ed00f) that goes to a dead end, is now replaced by the Clan Shatter-Shield navmesh's High Road extension coming down the new stairs added there, thus creating edge link changes to this navmesh.

#### Navmesh Sunk Below Ground
```
Cell: (31, 0)
NAVM Record: 000ed002

Cell: (31, 10)
NAVM Record: 000ed00f
```
* These two navmesh are small stubs that covered route around the side of Hjerim to a dead end. The mod adds a stair there to the high road. This connection allowed the High Road extension of the Clan Shatter-Shield navmesh to come down, replacing these two navmesh, to connect to Hjerim Navmesh. These two navmesh thus have been "disabled" by being sunk far beneath the ground. These two navmesh are small and had negligible role even in the vanilla game.

### Tamriel Worldspace

There is only one navmesh that EWE overrides in the Tamriel worldspace. All other expanded city footprint are currently handled with `L_NAVCUT` boxes, in regions where NPC traffic is usually pretty rare.

#### Northeast Gate Navmesh
```
Cell: (34, 10)
NAVM Record: 000fc12b
```
* The new Northeast Gate now has a door triangle on this navmesh. A few vertices have also be repositioned slightly.

### Windhelm Pit Worldspace

#### Windhelm Pit Navmesh
```
NAVM Record: 00040428
```
* The restored Windhelm Arena door link now has a door triangle on this navmesh.
