- [Epic Windhelm Expansion](#epic-windhelm-expansion)
  - [Summary](#summary)
  - [Compatibility](#compatibility)
  - [Permissions and Credits](#permissions-and-credits)
  - [Screenshot Details](#screenshot-details)

# Epic Windhelm Expansion

## Summary

**Epic Windhelm Expansion** (EWE) is my attempt at a Windhelm expansion mod for Skyrim Special Edition. Broadly speaking, **Epic Windhelm Expansion** tries to do the following.

* Expands Windhelm's footprint to the west, north, and northeast directions, resulting in 3 entirely new city districts.
* Restores and makes accessible the wallwalks of Windhelm, turning them into a picturesque and functional road network to be used by player and NPCs.
* Enhances the city walls with epic-looking battlements, towers, gatehouses, and castles, complete with crenelations with machicolations underneath.
* Adds many epic structures and scenic environments to the expanded Windhelm city in a way that boosts the overall level of atmosphere of Windhelm while remain lore-friendly and not overdone.
* Repairs many problems in the vanilla Windhelm worldspace, such as missing or hidden terrains, missing standins between worldspaces for close up cells not covered by LODs, and various other imperfections in vanilla buildings when viewed from nonstandard angles. Overall, the fixes should make the Windhelm worldspace look whole and complete from any vantage point in the newly expanded city.
* Provides a northeastern gate that exits to the flat snowy plains to Windhelm's northeast, allowing a more sensible and straightforward road to Winterhold to be paved.

For a quick basic walk-through of mod details (with lots of pretty pictures), as well as a starting point to dive into individual pages regarding added content, please read [the overview document](/docs/overview.md).

## Compatibility

The following articles give you an idea about compatibility with other mods:

* [Vanilla Area Edits in EWE](vanillaedits.md)
* [Vanilla Navmesh Changes in EWE](navmesh.md)

Since EWE keeps navmesh edits to an absolute minimum, many Tamriel Worldspace mods should be compatible out of the box. For example, I believe all Windhelm Bridge mods should be compatible. Other mods that may have minor object clippings in the Tamriel worldspace might be handlable by disabling some stuff via the console. There should not be navmesh shenanigans unless we're talking about the cell just outside the [Northeast Gatehouse](locations/northeastgatehouse.md) where I _do_ have a navmesh override (for the door triangle of the gate).

I currently have a patch for [Windhelm Exterior Altered](https://www.nexusmods.com/skyrimspecialedition/mods/5824). Though I will warn that this mod contains deleted navmeshes and therefore it is difficult for me to recommend, though it is a very popular mod for Windhelm (likely due to how few Windhelm area mods there are).

Large-scale "city overhaul" mods in the Windhelm Worldspace, such as JK's Skyrim or Capital Windhelm Expansion, are currently incompatible. Smaller mods that adds interiors might also be incompatible if door triangles are finalized onto a [vanilla navmesh edited by EWE](navmesh.md). The safer thing to do is usually to load EWE after other mods, since EWE's navmesh edits usually expands the navmesh shape into new areas and thus pretty crucial to keep, whereas if the other mods don't get their door triangles, the worst that can happen is followers stuck outside when you enter the mod-added house.

Of course, to make it perfect, a patch is required. I have patches planned for the future, though I don't know when I will get to them. Please see the [roadmap](roadmap.md) for details.

## Permissions and Credits

Other than inherited custom assets permissions discussed below, EWE itself has completely open permissions. You may do whatever you like with the mod. All I ask is a credit somewhere.

EWE contains custom assets from the following sources:

* [Manor House Modders Resource](https://www.nexusmods.com/skyrim/mods/61730) by [Tlaffoon](https://www.nexusmods.com/skyrim/users/6797977)
  * Some chimney pieces were retextured and used as roadside curb
* [Castle Modders Resource](https://www.nexusmods.com/skyrim/mods/61709) by [Tlaffoon](https://www.nexusmods.com/skyrim/users/6797977)
  * [Western Fort](locations/westernfort.md), [Northeast Castle](locations/northeastcastle.md), [Eastern Tower](locations/easterntower.md), [Dockside Keep](locations/docksidekeep.md) are built out of retextured models from here.
* [Stormcloak Cabin Resource](https://www.nexusmods.com/skyrim/mods/66514) by [Kraeten](https://www.nexusmods.com/skyrim/users/1876267)
  * Numerous buildings and facades in the [Northeast District](northeastdistrict.md) are retextured versions of this building. Also "hid" this building inside other buildings in order to have the "wolf head" wooden beams protrude out as decoration.
* [Griffon Fortress Resource](https://www.nexusmods.com/skyrim/mods/41036) by [mr_silka](https://www.nexusmods.com/oblivion/users/112530), converted by [Kraeten](https://www.nexusmods.com/skyrim/users/1876267)
  * Some pedestals for statues, general platforms, and stone stairs, were retextured models from here.
* [Black Thorn Keep](https://www.nexusmods.com/skyrimspecialedition/mods/25920) by [mmccarthy4](https://www.nexusmods.com/skyrimspecialedition/users/6075618)
  * The crenelations with machicolations for the battlements, as well as some wooden rampart rail, were retextured models from here.
* [Strotis Castle Wall Resource](https://www.nexusmods.com/skyrimspecialedition/mods/3152) by [stroti](https://www.nexusmods.com/skyrim/users/1657719), converted by [Tamira](https://www.nexusmods.com/skyrimspecialedition/users/776954)
  * The main structure of numerous wall towers and bastions were a retextured model from here.
* [Rochester SE (Mod Resource)](https://www.nexusmods.com/skyrimspecialedition/mods/28758) by [breti](https://www.nexusmods.com/skyrim/users/936576) ported to SSE by [sumugi](https://www.nexusmods.com/skyrimspecialedition/users/7464912).
  * The architectural meshes of the [Windhelm Bakery Building](locations/bakery.md), the "Rochester-style" buildings in the [Northeast District Residences](locations/northeastdistrictresidences.md), the [Meadhall](locations/meadhall.md)'s underbridge portion, the [Western District Temple](locations/westerndistricttemple.md)'s skyway, the new 03 building of the [Arena Market Shops](locations/arenamarketshops.md), the stone facades of the [Windhelm Arena Structural Restoration](locations/arena.md), among others.
* [Capital Windhelm Expansion](https://www.nexusmods.com/skyrimspecialedition/mods/42990) by [Surjamte](https://www.nexusmods.com/skyrimspecialedition/users/5215092)
  * Reused Surjamte's work on adding the eastern archway opening to the Palace of the Kings model.
* [Lakeview Manor Evolution - Source Files](https://www.nexusmods.com/skyrim/mods/41645) by [mrpdean](https://www.nexusmods.com/skyrim/users/5650734)
  * The front stairs of [Windswept Manor](locations/windsweptmanor.md), among other things.
* [Whiterun house foundations](https://www.nexusmods.com/skyrim/mods/55654) by [elinen](https://www.nexusmods.com/skyrim/users/934000)
  * The base foundation of [Windswept Manor](locations/windsweptmanor.md), among other things.
* [Tread Crane](https://www.nexusmods.com/skyrim/mods/62075) by [Tlaffoon](https://www.nexusmods.com/skyrim/users/6797977)
  * Treadmill cranes in [Dockside](locations/dockside.md)

Though EWE itself has completely open permissions, please know that the above custom assets have their own permissions that must be followed. EWE only uses third party assets that have open permissions, so that forks of EWE downstream does not have to re-ask for permissions. That said, third party assets permissions may still be more strict that EWE's permissions, so anyone forking EWE should keep that in mind. At the very least, make sure to:

* Always credit original authors of above assets
* Do not use in works released on sites other than NexusMods
* Do not use in works for monetary gain (e.g. no mods being sold, no donation points)

## Screenshot Details

The screenshots are taken using the following visual setup (in no particular order):

* [Silent Horizons ENB for Cathedral Weathers](https://www.nexusmods.com/skyrimspecialedition/mods/21543)
* [Enhanced Lights and FX](https://www.nexusmods.com/skyrimspecialedition/mods/2424)
* [Smooth Sky Mesh](https://www.nexusmods.com/skyrimspecialedition/mods/18350)
* [Cathedral Weathers and Seasons](https://www.nexusmods.com/skyrimspecialedition/mods/24791)
* [Obsidian Mountain Fogs](https://www.nexusmods.com/skyrimspecialedition/mods/13539)
* [Realistic Water Two](https://www.nexusmods.com/skyrimspecialedition/mods/2182)
* [Noble Skyrim Mod HD-2K](https://www.nexusmods.com/skyrimspecialedition/mods/21423)
* [Majestic Mountains](https://www.nexusmods.com/skyrimspecialedition/mods/11052)
* [Majestic Mountains Northside](https://www.nexusmods.com/skyrimspecialedition/mods/27981)
* [Blended Roads](https://www.nexusmods.com/skyrimspecialedition/mods/8834)
* [Cathedral Landscapes](https://www.nexusmods.com/skyrimspecialedition/mods/21954)
* [Enhanced Vanilla Trees](https://www.nexusmods.com/skyrimspecialedition/mods/11008)
* [Cathedral Plants](https://www.nexusmods.com/skyrimspecialedition/mods/26104)
* [Cathedral Snow](https://www.nexusmods.com/skyrimspecialedition/mods/18033)
* [Fluffy Snow](https://www.nexusmods.com/skyrimspecialedition/mods/8955)
* [ETHEREAL CLOUDS](https://www.nexusmods.com/skyrimspecialedition/mods/2393)
* [Better Dynamic Snow SE](https://www.nexusmods.com/skyrimspecialedition/mods/9121)
* [Skyrim Textures Redone - Enhanced Night Sky](https://www.nexusmods.com/skyrimspecialedition/mods/5561)

Screenshots were taken with `uGridsToLoad=9` with xLODGen for terrtain LOD + TexGen + DynDOLOD at the "Medium" setting. `uGridsToLoad=9` is not a value that should be used to actually play the game. In practice, LOD generation should be good and sufficient. See the [Standins and LODs](standinslods.md) page for details on how these are handled and what users are expected to do.

A very small number of screenshots (some night shots) were doctored slightly in Photoshop where contrast is dialed down and brightness is dialed up. For some reason the screenshots come out very dark despite it looking fine in game.
