- [EWE Roadmap](#ewe-roadmap)
  - [Goals for Releases beyond 0.3.0:](#goals-for-releases-beyond-030)
  - [Goals and Expectations Beyond 0.3.0](#goals-and-expectations-beyond-030)
    - [High Priority Goals](#high-priority-goals)
    - [Standalone Content Projects](#standalone-content-projects)
  - [Backlog (Anticipated Future Work)](#backlog-anticipated-future-work)
    - [Continuous Bug Fixes and Quality Improvements](#continuous-bug-fixes-and-quality-improvements)
    - [Windhelm Worldspace Idle Markers](#windhelm-worldspace-idle-markers)
    - [Gatehouse Portcullises and Murder Holes](#gatehouse-portcullises-and-murder-holes)
    - [Road Signs Inside Windhelm](#road-signs-inside-windhelm)
    - [Northeast Road to Winterhold](#northeast-road-to-winterhold)
    - [Windhelm Docks Expansion](#windhelm-docks-expansion)
    - [Eventual Patches With Other Mods](#eventual-patches-with-other-mods)
      - [JK's Skyrim / Dawn of Skyrim](#jks-skyrim--dawn-of-skyrim)
      - [Fortified Windhelm](#fortified-windhelm)
      - [Capital Windhelm Expansion](#capital-windhelm-expansion)
      - [Faction: Pit Fighter](#faction-pit-fighter)
      - [AI Overhaul](#ai-overhaul)
      - [Bells of Skyrim](#bells-of-skyrim)
      - [Windhelm Docks Pathways](#windhelm-docks-pathways)
      - [Vigilant](#vigilant)
      - [The Elder Scrolls Online Imports](#the-elder-scrolls-online-imports)
      - [A Note on Open Cities Skyrim](#a-note-on-open-cities-skyrim)

# EWE Roadmap

[Release Notes for past releases](releasenotes.md)

## Goals for Releases beyond 0.3.0:

* Northeast road to Winterhold (as an addon ESP)
* xEdit script for patching door triangles between addons
* Windhelm worldspace idle markers
* Finalize area between buildings and the wall in the Northern District

## Goals and Expectations Beyond 0.3.0

The next bit of work will start to get into interiors, NPCs, quests, etc...

### High Priority Goals

* Build out the architectural "shells" of the interior spaces, such that at the very least the walls, floors, and ceilings are all there when entering one of the interior cells. The hope is that it could lower the barrier for folks to create interior addons, if they are mostly interested in interior decoration. Folks who want to complete rebuild the interior can always disable the existing walls, floors, and ceiling pieces without too much hassle.
* Add idle markers to the Windhelm worldspace. This is something used to be a 0.3.0 goal but I've pushed it to beyond. Idle markers are important foundations for eventually having NPCs be able to idle in some location in their daily schedules.

### Standalone Content Projects

"Military" content will be prioritized over "civilian" content. "Military" content are content related to the [Battlements of Windhelm](locations/battlements.md). The initial goal will be to get guards patrolling the walls.

Then, "communal" buildings will be prioritized over individual residences and shops. "Communal" buildings are where NPCs that live or work in other buildings are likely to visit. Examples include [The Meadhall](locations/meadhall.md), [Northeast District Pub](locations/pub.md), [Windhelm Bathhouse](/locations/bathhouse.md), and [Windswept Manor Commons](/locations/windsweptmanorcommons.md). The initial goal is to make it so that any NPC can be told to go idle in one of these buildings as part of their daily schedule, and they would do so believably.

The first pass at interiors and NPCs will result in barebones interiors with the most generic or "inconsequential" NPCs. Subsequent passes will add clutter to beautify interiors, and upgrade NPCs into more complex NPCs with custom dialogue, and possibly some with custom voices.

## Backlog (Anticipated Future Work)

### Continuous Bug Fixes and Quality Improvements

There are always going to be these.

* Bug fixes and quality fixes as I notice them (or as people report them)
* Further optimized use of occlusion primitives
* Further minor cosmetic tweaks
* Further environment tweaks relating to lighting, sound, etc...
* Further optimization regarding snow shader/texture shenanigans
* Cleaning of redundant and unused assets and object records that have accumulated over time

### Windhelm Worldspace Idle Markers

I need to do a pass to add idle markers, such as wall leaning markers, to all expanded locations in the Windhelm Worldspace, so that NPCs told to idle somewhere will find things to do.

### Gatehouse Portcullises and Murder Holes

Portcullises and murder holes should be an easy addition to at least the [Northeast Gatehouse](locations/northeastgatehouse.md) if not also the [South Gatehouse](locations/southgatehouse.md). They don't have to be functional and only need to look like they are there.

### Road Signs Inside Windhelm

Vanilla Windhelm has always been a confusing city that's easy to get lost in. With EWE, though navigation is arguably made easier by the Wallwalk roads, we nevertheless have more area in general. Thus I think it would be good if there are signs here and there in the city that tells you the general direction of districts and landmarks.

This should only involve creating the sign statics, and then putting them up along various walls and corners in the city. Purely additive objects.

### Northeast Road to Winterhold

The base mod adds a [Northeast Gate](locations/northeastgatehouse.md) that opens to the snowy plains beyond. What I want to happen is to pave a new, more straightforward road from there to Winterhold. The identified path to pave the road through can be seen in the below image.

![](/images/roadtowinterhold.png)

Probably also need to add road signs to match at junctions.

This will be a complete separate addon ESP.

### Windhelm Docks Expansion

This is not a high priority. The main city content will come first. That said, I once sketched out some ideas regarding a docks expansion outside the Gray Quarter walls, outlined by the below image.

![](/images/docksideexpansionidea.png)

### Eventual Patches With Other Mods

#### JK's Skyrim / Dawn of Skyrim

I expect to look at the JK+DoS patch work in-depth at some point, but again for now the city contents come first. I understand that JK's and DoS are extremely popular mods and patches should be made for them.

However, having created plenty of patches at this point, I also understand that patches can be extremely brittle and comes with heavy maintenance costs, so the return in value should be worth the work. I also looked at what JK and DoS does to Windhelm, and decided that I like about maybe 60-70% of what I see. I also looked at the JK+DoS combined patch, and decided that the end result just feels really cluttered. Granted, right now I'm just remembering vague impressions, and I should probably go back and re-confirm these impressions.

Nevertheless, eventually when I get to the JK+DoS problem, I am expecting one of two solutions. The first is a straight up patch between EWE base and JK's, EWE base and DoS, and EWE base and JK's + DoS. If I go with this solution, I might end up ditching support for the JK's + DoS case because the complexity might not be worth the actual aesthetic value it brings. Others of course are free to make any patches they like, or fork my mod however they like, etc...

The second potential solution is for me to go with the Enhanced Solitude route, where I will have EWE (or perhaps an addon to EWE) handle the clutter work for vanilla areas, and then have players who wish to use JK's and/or DoS to simply eliminate Windhelm from the places JK's/DoS covers. This isn't the nicest solution, but I think the fault would then lie on JK's and DoS for not providing their mods in modular fashion, especially when modular versions originally existed in Skyrim LE.

If I go with this second solution, I will probably try to copy in the main aesthetics JK's and DoS I liked. I think what I liked are probably close to what other users like, and what I didn't liked are probably what the users typically find "meh" as well, so hopefully I would still provide the important stuff people expect from JK's and DoS. The last thing worth mentioning is that today, people surgically remove cities from JK's and DoS by following an xEdit guide; it's a rather involved process and most less technically-inclined users would not be able to do. I've always thought that a tool can probably be made for this process, so I will probably try to write such a tool so users can easily delete Windhelm (or any other city) from their JK's + DoS.

#### Fortified Windhelm

The author of upcoming highly-anticipated mod Fortified Windhelm, [skyfall515](https://forums.nexusmods.com/index.php?/user/41181730-skyfall515/), has contacted me regarding compatibility. In our conversation it seems decided that Fortified Windhelm will be split into two pieces, such that the bridge portion can be standalone. This way, the bridge of Fortified Windhelm can seamlessly connect to EWE, while the towers and ramparts would serve as an alternative to EWE.

#### Capital Windhelm Expansion

[Capital Windhelm Expansion](https://www.nexusmods.com/skyrimspecialedition/mods/42990) (released Jan 17, 2021) by [Surjamte](https://www.nexusmods.com/skyrimspecialedition/users/5215092) is currently the best Windhelm expansion mod there is. I would very much like to patch EWE with this mod, and there are no legal or social barriers to do so as Surjamte has completely open permissions for any kind of patch as well as asset usage.

From what I can see, a patch is theoretically possible, but will take a lot of work. These are my current thoughts at a glance:

* In the Windhelm worldspace, try to incorporate content from CWE where possible (such as CWE's Gray Quarter), but overall keeping to EWE's content in heavily conflicting areas in order to fit the theme/aesthetics of EWE's walls and footprint. In some places, major rebuilds could try to merge content from both mods (such as market square). In other places, EWE's content would be picked wholesale over CWD (such as northeast district). Resolving heavily conflicting areas may involve automated work being done by xEdit scripts in several strategic passes.
  * How much can be reused will also depend on how much CWE has changed the vanilla layout of the city, and also the modularity of the new assets he has made. For example, I wonder whether some of his new graveyard meshes can be a drop-in replacement for the vanilla graveyard, or whether they are all tightly coupled with his new layout. Challenges like these will be discovered when I eventually start looking at it in depth.
  * Consider replacing EWE's northern district with CWE's extension in that part. This might not work out due to the positioning of EWE's northern wallwalk, and the result might look sloppy anyway due to jarring transition between "neat and organized" EWE areas and more "naturally well-worn" CWE area, but it should be tried, since a success would mean more CWE content remains and less work relocating CWE's NPCs and interiors in that area.
* In addition to the above, interiors added by CWE can be patched to EWE's doors. For example, one of the two bakeshops can be patched to EWE's designated bakery door. House of various NPCs can be patched to EWE's various residences, etc...
* The above will mean repositioning all quest-related content to match. This is likely the most complex part since it requires detailed understanding of how the quests work.

Though the patch is planned, please note that EWE is not near content completion, and it will likely take months if not another year to get to the point where EWE can stand on its own. Overall, EWE content will have priority over most patches, including this one.

#### Faction: Pit Fighter

EWE restores the arena structure, but does not yet touch any playable content. [Faction: Pit Fighter](https://www.nexusmods.com/skyrimspecialedition/mods/40094) by ThirteenOranges is the forefront mod that restores the Windhelm Arena quest content. I am hoping to create a patch at some point to be able to bring that aspect into EWE and make the arena playable with EWE.

That said, personally I have never played through Faction: Pit Fighter, so I do not know how the content works, whether such a patch actually makes sense and does what I'm expecting, and what all the conflicts are.

#### AI Overhaul

This mod was mentioned as a potential patch candidate, so I will look at this at some point. Not yet sure whether a patch is easy or difficult or needed at all. I think it will depend on what it does to navmesh, if any.

#### Bells of Skyrim

This mod places a big alarm bell platform in (what I believe to be, based on vague screenshots) the prison tower location, which means it should be conflicting with the Windhelm Arena and possibly Windhelm Arena Market content. At some point I will look at this and provide a patch. The patch will likely attempt to relocate the bell tower elsewhere. Apparently the Enhanced Solitude author did it for the Solitude bell, so moving those bells is probably a good solution.

#### Windhelm Docks Pathways

At some point, I will look at creating a patch for Windhelm Docks Pathways. If a direct patch is not easy due to object conflicts being messy, I may instead fasttrack my own docks expansion idea (see above relevant section) to provide an alternative for making that link.

#### Vigilant

It sounds like Vigilant adds an "old well that serves as a shortcut to underground windhelm (new area)". This well I think likely conflicts with EWE's new footprint, so a patch is needed to move it further outside. That said, even without the patch, it sounds like this is a "shortcut"? As in, the underground Windhelm area can still be accessed by way of Palace of the Kings' guard barracks, so it might not be a gameplay blocker. I have no idea, and would appreciate additional information from someone more familiar with Vigilant.

#### The Elder Scrolls Online Imports

This mod adds a bunch of ruined buildings and a "Hall of Trials" building to the area covered by EWE's Northeast District. The conflict creates some jarring clipping at EWE's northeast walls, and makes the Hall of Trials inaccessible (and likely causes navmesh shenanigans). These conflicts feel like they should be easy to resolve so at some point I will work on a patch.

#### A Note on Open Cities Skyrim

I do not plan to work on an Open Cities patch. I personally have no interest in Open Cities Skyrim, and can't recommend it for anyone with a large load order since it generally requires a lot of patches and has stricter requirements on having child worldspace content match with parent worldspace content.

That said, my mod has fully open permissions, which in turn means anyone is free to try to make an OCS patch, assuming they can solve any technical challenges involved.

Currently, as of version 0.3.0, the Windhelm worldspace content of this mod should be mostly matching with Tamriel worldspace standins. In fact, standin generation is done automatically with a script, so you can be sure that the standins have the exact footprint as the canonical objects they are standins of.

There are some remaining issues an OCS patch would need to solve.

For example, some vanilla models have inaccurate collision data, making it so that even when they are placed out of sight "behind" other structures, their out-of-place collision boxes could still result in invisible "walls" or "platforms" the player can experience. For these, I made collision-less versions of the model by deleting the collision geometry in NifSkope, and used them in the Windhelm worldspace. This was sufficient because the offending objects are almost always large wall pieces that the player would see in the Windhelm worldspace but never go close enough to bump into.

Similarly, I remodeled some vanilla wall assets by hand-adjusting vertices in NifSkope in order to keep their outer facades but remove the weird corners and edges that ate into places that model had no business to be in. For these assets, I removed collision for the Windhelm worldspace version, but kept the original asset for the Tamriel worldspace version since in the Tamriel worldspace, city interior details don't matter.

In an OCS world, my solutions would not suffice, and the patch maker would need to properly redo collisions in Blender or 3ds Max.

Finally, there is the issue with the mismatched position of the vanilla Docks gate between the worldspaces. My mod does not change the location of the gate, however it built structures around the gate in a way that it would work for both positions. An OCS patch would need to address this incongruency for real instead of working around it, and in doing so, may need to redo my gate structures a bit.
