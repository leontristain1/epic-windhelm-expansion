- [Version 0.3 "Exterior Enhancements"](#version-03-exterior-enhancements)
  - [Version 0.3.0](#version-030)
    - [Technical Changes](#technical-changes)
    - [Notable New Content](#notable-new-content)
    - [Minor Bug Fixes and Enhancements](#minor-bug-fixes-and-enhancements)
- [Version 0.2 "Battlements"](#version-02-battlements)
  - [Version 0.2.0](#version-020)
    - [Notable New Content](#notable-new-content-1)
    - [Minor Bug Fixes and Adjustments](#minor-bug-fixes-and-adjustments)
    - [Performance Expectations](#performance-expectations)
- [Version 0.1 "Northeast District"](#version-01-northeast-district)
  - [Version 0.1.0](#version-010)
  - [Version 0.1.1](#version-011)
  - [Version 0.1.2](#version-012)
- [Version 0.0 "Initial Release"](#version-00-initial-release)
  - [Version 0.0.1](#version-001)
  - [Version 0.0.2](#version-002)

# Version 0.3 "Exterior Enhancements"

## Version 0.3.0

### Technical Changes

* Removed "Is Full LOD" flags added in version 0.2. LODs are now supported only via DynDOLOD. The mod now contains LOD copies of custom models, and a proper DynDOLOD config file. The mod has been tweaked to ensure DynDOLOD output looks correct.
  * Note that this means without running DynDOLOD, you will no longer see LODs for the expanded city. You are now expected to run DynDOLOD if you expect LODs.
* Standin and LOD management is now done with an xEdit script and a "labels" plugin file. The workflow is something like 1) label various models of the mod via labels plugin overrides in the CK, and 2) run both the main mod and the labels plugin through the xEdit script. The xEdit script will automatically create/update standin copies across worldspaces, and ensure LOD nif copies in the asset files, all based on the labels. Both the script and the labels file are bundled in the mod archive, though it should be understood that these are for modders (i.e. me, or whomever forks this mod) to use, and not for users.

### Notable New Content

* The [Meadhall](locations/meadhall.md) and [Windhelm Bakery](locations/bakery.md) have been expanded into one continuous structure that now encompasses the entire underside of the [High Road](locations/highroad.md) bridge.
* The [South Wall Tower](locations/southwalltower.md) has been extended across the full width of the [Southern Wallwalk](locations/southernwallwalk.md). The wallwalk runs through two sets of open archways. The structure is outfitted with portcullises and murder holes.
* The frontage of the [Windswept Manor](locations/windsweptmanor.md) has been reworked. The stairs leading up to the main doors now open sideways, thus creating a room for a proper open area in front. A large horizontal gate now exists at the very bottom of Windswept Manor, leading into a new [Windswept Workshop](locations/windsweptworkshop.md) interior.
* The stairs that leads up to the [Beacon of Windhelm](locations/beacon.md), which used to be a series of wooden stairs, have been swapped for stone steps lined with large log pieces. The mountainside is now a small forest with tall trees, giving the pathway a more secluded and natural feeling.
* The pathway up north from [Windswept Manor](locations/windsweptmanor.md) has been reworked into a series of neater steps, and now forks off a path to the right, around the base of the [Beacon of Windhelm](locations/beacon.md) cliff, over to a side entrance of the [Western District Temple](locations/westerndistricttemple.md).
* The [Western District Temple](locations/westerndistricttemple.md) is now connected directly to the [Mountain Caves](locations/mountaincaves.md) via a "Bridge of Sighs"-like skyway, that hangs above the bathhouse alley.
* A new [Northern District Temple](locations/northerndistricttemple.md) in the style of Sovngarde has been added to the [Northern District](locations/northerndistrict.md) in place of the old [Stonemasonry Guild](locations/stonemasonryguild.md). This temple is now one of the major landmarks in Windhelm, and the likely place for Windhelm's eventual main temple. The Stonemasonry Guild itself has been moved to replace the old 01 entrance of the [Northern District Residences](locations/northerndistrictresidences.md).
* The bonfire has been removed from the old bonfire plaza, in favor of a series of braziers surrounding a statue of Ysgramor, as decor that matches better with the new temple.
* The [Windhelm Arena](locations/arena.md) has a new outer appearance made up of large arches and dark stonework.
* The front courtyard of the [Windhelm Arena](locations/arena.md) is now directly connected to the Palace of the Kings courtyard and the [Windhelm Arena Market](locations/arenamarket.md), making that whole area much more open.
* The lower area of the [Windhelm Arena Market](locations/arenamarket.md) now has a third shop frontage underneath the stone bridge.
* The 01 and 02 buildings among the [Northeast District Residences](locations/northeastdistrictresidences.md) have been swapped from Brunwulf-style buildings to Rochester-style buildings, providing more architectural variety for that area of the city.
* The [Dockside Keep](locations/docksidekeep.md) has been completely redone. It is now a physically imposing structure with vertical rows of windows somewhat resembling the Lighthouse of Alexandria. A number of treadmill cranes have been added to the [Dockside](locations/dockside.md).
* The Docks Gate has been reworked a bit in order to allow the overpass above it in the Windhelm worldspace to show up nicely in the exterior worldspace as standins, despite the vanilla positions of the gate itself being inconsistent between the two worldspaces.
* Changes made in `EpicWindhelm-ltrNorthernMountains.esp` is now included in the base mod. Since EWE's base mod does have a very strong opinion on the Tamriel worldspace footprint by way of standins, the case for this plugin as a separate optional plugin is not very strong. Note that one change made here is to slightly lower a mountain mesh nearby, which will have a slight effect on a mountain peak slightly north of Windhelm. This change is currently not expected to be a source of conflict and is not expected to affect the typical player experience.

### Minor Bug Fixes and Enhancements

* The Arena Prison Tower and the similar structure used for the [Northeast District Castle](locations/northeastdistrictcastle.md) now have some snow-covered roofs added.
* A valunstrad architecture piece (opposite of stairs that go up to the [Northern District](/locations/northerndistrict.md)) now has more accurate collisions and some filler walls. An existing collision box in the area has also been lowered below ground. The result is that you can actually jump onto it without landing on a big invisible box. Unfortunately, the backside of a nearby house still has really bad vanilla collisions that are not yet fixed, so it's still not a perfect experience, but should be better than before.
* Better-textured wall pieces are used to cover the base wall for the easternmost houses of [Northeastern District Residences](locations/northeastdistrictresidences.md). This should make it so that when player enters the city from the [Northeast Gate](locations/northeastgatehouse.md), they no longer see bad textured wall.
* Chimneys with smoke have been added to the [Windswept Manor](locations/windsweptmanor.md) and the various buildings of the [Western District Residences](locations/westerndistrictresidences.md), as well as the [Windhelm Bakery](locations/windhelmbakery.md).

# Version 0.2 "Battlements"

## Version 0.2.0

### Notable New Content

* Overhauled the wallwalks with battlements
  * [Southern Wallwalk](locations/southernwallwalk.md) is now fitted with the [South Wall Tower](locations/southwalltower.md), 2 catapult bastions, and "hoardings-like" wooden coverings that protect folks from the elements.
  * The vanilla south gate is now fitted with the [South Gatehouse](locations/southgatehouse.md), with the wall-level passageway redone.
  * The wallwalk stretch between the South Gatehouse and the [Dockside Area](locations/dockside.md) has been completely redone, now fitted with the [Dockside South Tower](locations/docksidesouthtower.md).
  * The southern end of the [Gray Quarter Wallwalk](locations/grayquarterwallwalk.md) is now fitted with the [Dockside East Tower](locations/docksideeasttower.md).
  * The [Eastern Tower](locations/easterntower.md) now has a wall-level circular platform that player and NPCs can use to walk "around the outside" instead of going through the center.
  * The [Northeast Gatehouse](locations/northeastgatehouse.md) and associated wallwalks is completely redone and is now cleaner both in layout and appearance.
  * The [Northeast Castle](locations/northeastcastle.md) has been completely redone, with an actual castle-like structure extending up from the existing structure, featuring two large balconies, one of which with roof access. There is now a main gate in the center of the Northeast Castle. The old upper entrance to the Northeast Castle has been made to lead into an independent "side room" interior cell, dubbed the [Northeast Castle Side Room](locations/northeastcastlesideroom.md).
  * Added the [Northern Wallwalk](locations/northernwallwalk.md), extending behind Palace of the Kings, from the Northeast Castle over to the [Mountain Caves](locations/mountaincaves.md).
  * All above wallwalks, towers, bastions, gatehouses, castles, forts, etc... are outfitted with proper crenelations with machicolations underneath.
* The "northern" section of [Gray Quarter Wallwalk](locations/grayquarterwallwalk.md), that led to the [Windhelm Arena](locations/arena.md), and separated the Gray Quarter from the [Northeast District](locations/northeastdistrict.md), has been turned into the [Windhelm Arena Market](locations/arenamarket.md) - an epic, long, covered market space. The "lower" portion in front of [Northeast District Temple](locations/northeasttemple.md) now has 2 interior storefronts and some market stands.
* The battlements overhaul and the [Windhelm Arena Market](locations/arenamarket.md) work resulted in some minor changes to interior door allocations, and some new interior cells:
  * [Northeast Castle](locations/northeastcastle.md) now has a main gate, two upper level balcony doors, and a door to Northern Wallwalk.
  * [Northeast Castle](locations/northeastcastle.md)'s old upper gate now leads into a new interior dubbed the [Northeast Castle Side Room](locations/northeastcastlesideroom.md)
  * New interior available from the [South Wall Tower](locations/southwalltower.md), featuring a wall-level main door and a roof door
  * Two new interior available from the [South Gatehouse](locations/southgatehouse.md), one for each of the gatehouse towers, each featuring a wall-level main door, a roof door, and a side door to access the outside platform above the gate. These are known as [South Gatehouse Tower 01](locations/southgatehousetower01.md) and [South Gatehouse Tower 02](locations/southgatehousetower02.md) respectively.
  * New interior available from the [Dockside South Tower](locations/docksidesouthtower.md), containing only a main gate. Roof access is done outside via stairs.
  * New interior available from the [Dockside East Tower](locations/docksideeasttower.md), containing only a main gate. Roof access is done outside via stairs.
  * Two new interiors available for the lower portion of the [Arena Market](locations/arenamarket.md), dubbed the [Arena Market Shops](arenamarketshops.md).
* [Beacon of Windhelm](locations/beacon.md) has been turned into a proper lighthouse beacon structure, complete with a circular platform base that offers good views.
* [Northern District](locations/northerndistrict.md)'s bonfire has been turned into a pyre-like structure that looks a bit more civilized and believable as a bonfire. Some market stands have been added around it.
* Added various torches and braziers as night lighting all over the expanded Windhelm
* A separate addon: `EpicWindhelmBase-ltrNorthernMountains.esp` adds a bunch more mountain cliffs to the mountains to the north of Windhelm, providing a much more vertical cliff face next to the [Northern Wallwalk](locations/northernwallwalk.md), making defense concerns there more believable.
* Various bridges and stairs around Windhelm has been swapped with more solid-looking stone models. Notably, this makes the "under the bridge" passage between the [Expanded Market](locations/expandedmarket.md) and the [Western District](locations/westerndistrict.md) a lot wider and more open.
* Updated Tamriel worldspace standins for all above objects. Many objects have enabled the "Is Full LOD" ("neverfade") flag in order to roughly provide some semblance of LODs. This could be a performance concern. In the next release we will properly address all LOD issues and make it DynDOLOD-friendly.

### Minor Bug Fixes and Adjustments

* [Northeast Castle](locations/northeastcastle.md) railing has been raised a bit to be more believable as a safety railing.
* Many ground meshes have been swapped for meshes that are more friendly to lighting, snow textures, etc...
* [Windswept Manor](locations/windsweptmanor.md)'s balcony railing, which used to be a bunch of overlapping ladders, have been replaced with a classier one based on a retextured whiterun railing.
* Added Editor IDs for unnamed Windhelm worldspace cells containing the expanded Windhelm.
* Replaced the "nordic ruins" stone stair objects with Windhelm-retextured versions.
* Replaced many large wood log structures (used as wallwalk "fences") with retextured versions with 4x the density, so they don't look so pixelated up close.
* Updated [Western Fort](locations/westernfort.md)'s texture mapping to have larger stone blocks similar in size to the walls themselves, giving the Western Fort a stronger and more integrated appearance.

### Performance Expectations

* In Windhelm worldspace, at `uGridsToLoad=5`, in the worst case we seem to be hitting about ~14000 draw calls. Note that this "worst case" tend to happen from high vantage points since at lower vantage points the occlusion planes kick in and make things a lot better. The common case is around 6000-7000 draw calls.
* In Tamriel worldspace, at `uGridsToLoad=5`, in the worst case we seem to be hitting about ~12000 draw calls. This is due to the neverfade flags being on to simulate LODs. Note that this "worst case" tend to happen from high vantage points since at lower vantage points the occlusion planes kick in and make things a lot better. The common case is around 5000-6000 draw calls.
* In comparison, worst case (from high vantage points) for Enhanced Solitude can get up to ~22000 draw calls, and for Whiterun Valley can get up to ~20000. Though do note that those mods have clutter and EWE currently does not.
* On my machine (i7-7700k 4.2GHz 8CPUs, GTX980 4GB VRAM, 32GB RAM, Full SSD, playing the game with Silent Horizons ENB and 2k and some 4k textures), I get ~45-50FPS in the worst case. This drops down to ~30FPS if I have other resource-intensive processes (such as CK) also running at the same time. In comparison, My game without EWE is usually in the 45-60FPS range with 30FPS worst case (such as in heavier areas of Whiterun Valley). The current conclusion is that the current state of EWE is certainly more demanding than the vanilla game, but not out of the norm for city expansion mods. Though it's worth noting that with clutter this can go a few thousand higher, and even more once patched with mods like JK's, but all that should only affect the Windhelm worldspace problem, and can still be combatted with further optimized occlusion (such as using occlusion boxes rather than occlusion planes, and using more of them).

# Version 0.1 "Northeast District"

## Version 0.1.0

* Windhelm Arena structural restoration
* Added Northeast District and Northeast Gate
* Added stub cells for interiors
* Removed Gray Quarter Gate from 0.0.1
* Built out the Windswept Manor Commons interior

## Version 0.1.1

* Fixed flickering textures at top of steps of Western District Temple
* Fixed issue where backside of Western District Residence 03 does not have collision geometry, allowing people to walk through. This was mostly solved with collision planes. The back edge is especially bad and was addressed with a pile of firewood and some planks.
* Removed some snow thickets from the market square and the stairs up to Dockside Keep
* patched a hole in the `WHDock1` nif using a `WHStonetop`
* included nifs for `whhjerim1`, `whmarket02`, `whmarket02destroyed`, `whclanshattershield` from USSEP; the USSEP fixes to the nifs are crucial to the mod looking right; we copy the nifs in order to avoid a hard dependency (USSEP permissions allows this).
* added collision planes behind Clan Shattershield home to prevent the player from being able to walk through some parts of the walls there

## Version 0.1.2

* Cleaned up unnecessary navmesh record overrides on vanilla windhelm worldspace navmeshes `000660c6` (temple of talos), `0002e50d` (clan cruel-sea), `000ed022` (clan cruel-sea), `000b8c54` (stone quarters), `00098204` (palace of kings), `0003030c` (Gray Quarter).
* Cleaned up unnecessary navmesh record overrides on vanilla Tamriel worldspace navmeshes `000164fa` (cell north of northeast gate), `000fc13f` (cell east of northeast gate).
* Fixed issue where previous attempt to revert the Gray Quarter gate was incomplete. Specifically, Hlaalu house door's teleport marker position was not reverted, and now it is. Also reverted positioning of a pile of firewood and a shrub.
* Fixed two floating icicles that were moved to new positions by USSEP.

# Version 0.0 "Initial Release"

## Version 0.0.1

* initial release

## Version 0.0.2

* Removed "Is Full LOD" from fort models because apparently that flag make them show up in Tamriel exterior which is bad. Unfortunately this means in Windhelm worldspace you won't see the fort until you get closer (it pops in like normal statics)
