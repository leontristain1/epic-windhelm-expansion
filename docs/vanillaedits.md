- [Vanilla Area Edits in EWE](#vanilla-area-edits-in-ewe)
  - [Windhelm Worldspace Vanilla Area Edits](#windhelm-worldspace-vanilla-area-edits)
    - [Southern Gate](#southern-gate)
    - [Expanded Market](#expanded-market)
    - [House of Clan Shatter-Shield](#house-of-clan-shatter-shield)
    - [Hjerim](#hjerim)
    - [Palace of the Kings - Valunstrad](#palace-of-the-kings---valunstrad)
    - [Arena Prison Building](#arena-prison-building)
    - [Stone Quarter - Dockside Stairs](#stone-quarter---dockside-stairs)
  - [Tamriel Worldspace Vanilla Area Edits](#tamriel-worldspace-vanilla-area-edits)
    - [Southern Walls](#southern-walls)
    - [South Gatehouse and Dockside South Tower](#south-gatehouse-and-dockside-south-tower)
    - [Northeast District and Battlements](#northeast-district-and-battlements)
    - [Northern Mountains](#northern-mountains)
    - [Ores and Wildlife](#ores-and-wildlife)

# Vanilla Area Edits in EWE

This document serves as information for folks who might want to patch the mod with other mods, or generally trying to debug problems in their own load orders.

## Windhelm Worldspace Vanilla Area Edits

In the Windhelm Worldspace, edits to vanilla areas are kept to a minimum. Though there are some edits, all are necessarily made for a crucial functionality, and overall there should not be too many.

### Southern Gate

The structure of the [Southern Gatehouse](locations/southerngatehouse.md) added some additional footprint to the walls. Then, along the [Southern Wallwalk](locations/southernwallwalk.md), wallwalk stairs were added next to the walls. Nearby, a pile of firewood was moved as a result of the wallwalk stairs.

![](/images/vanillaedit1.png)

### Expanded Market

In the [Expanded Market](locations/expandedmarket.md) area, additional wallwalk stairs were added along the walls.

Since the market area is no longer a dead end, two market stands, including the produce stand of Hillevi Cruel-Sea, have been relocated to the expanded side of the market square. This is so that the stands don't block the way towards the [Western District](locations/westerndistrict.md). Adding some life to the new expanded corner is also a plus.

![](/images/vanillaedit2.png)

### House of Clan Shatter-Shield

The left side of the House of Clan Shatter-Shield courtyard (used to be a dead end) now has a flight of wooden stairs that accesses the [High Road](locations/highroad.md).

![](/images/vanillaedit3.png)

### Hjerim

The left side of the Hjerim courtyard (used to be a dead end) now has a small set of wooden stairs that accesses the [High Road](locations/highroad.md).

![](/images/vanillaedit4.png)

### Palace of the Kings - Valunstrad

The passage that leads out west from the Palace of the Kings courtyard and into Valunstrad used to have a non-functional structure to its immediate north. That structure has been replaced now with stairs up to the [Northern District](locations/northerndistrict.md).

![](/images/vanillaedit5.png)

### Arena Prison Building

As a result of the structural restoration of the [Windhelm Arena](locations/arena.md), the vanilla prison tower, containing an `L`-shaped passage between the Palace of the Kings courtyard and the Aretino Residence, now has the unused prison tower made for the Arena in its place. The `L` shaped passage is now a `+` shaped cross-passage, with the original two `L` directions retained. The two new directions leads north into the Arena courtyard, and east into the [Windhelm Arena Market](locations/arenamarket.md).

![](/images/vanillaedit6.png)

### Stone Quarter - Dockside Stairs

A flight of stairs have been added close to the Aretino Residence and Brunwulf Free-Winter's House, that ascends up to access the [Dockside](locations/dockside.md) area as well as over the Dock Gate to access the [Gray Quarter Wallwalk](locations/grayquarterwallwalk.md). The stairs are placed next to some broken stones, making it look like the stairs itself is in a state of disrepair.

![](/images/vanillaedit7.png)

## Tamriel Worldspace Vanilla Area Edits

In the Tamriel Worldspace, the footprint of the expanded city is retained. Nothing else changes, so this is all about where the footprint of the expanded city are now at.

### Southern Walls

The southern walls of the city now extends all the way west until it gets anchored into the mountain by the [Western Fort](locations/westernfort.md). This encloses the space for the [Western District](locations/westerndistrict.md) and [Northern District](northerndistrict.md), which are no longer accessible from the Tamriel Worldspace.

Also along the way are catapult bastions and various towers. Together, they made it so that the walls are now a bit more south than the vanilla wall's level.

![](/images/vanillaedit8.png)

### South Gatehouse and Dockside South Tower

The [South Gatehouse](locations/southgatehouse.md) and the [Dockside South Tower](locations/docksidesouthtower.md) also protrudes out a bit from vanilla Windhelm's footprint. Luckily, it seems they protrude out onto cliffs or existing structures of the dock mesh, so they are unlikely to collide with other mods that add content here.

![](/images/vanillaedit9.png)

### Northeast District and Battlements

The added footprint of the [Northeast District](locations/northeastdistrict.md) as well as its various battlements starts with the footprint of the [Eastern Tower](locations/easterntower.md) and extends over to the footprint of the [Northeast Castle](locations/northeastcastle.md).

In this area, some rocks and trees that ended up being in front of the [Northeast Gatehouse](locations/northeastgatehouse.md) were moved over to the side. There were also some mining resources and animal spawns within the footprint of the Northeast District, that were relocated further to the northeast.

![](/images/vanillaedit10.png)
![](/images/vanillaedit11.png)

### Northern Mountains

The mountain cliffs north of the [Northern Wallwalk](locations/northernwallwalk.md) have been modified a bit, in order to provide a more vertical cliff face. Part of the modification also changed the position of one of the large mountain peaks slightly. That said, the changes should still be a minor cosmetic change, where the new position should not affect any navmeshed mountain paths in the area.

![](/images/vanillaedit-northernmountains1-before.png)
![](/images/vanillaedit-northernmountains1-after.png)
From the other direction:
![](/images/vanillaedit-northernmountains2-before.png)
![](/images/vanillaedit-northernmountains2-after.png)

### Ores and Wildlife

The [Northeast District](locations/northeastdistrict.md) enclosed some vanilla areas that had some ores and wildlife markers (wolves) in the Tamriel worldspace. Those ores and markers have been relocated further northeast to make them still available. They should not have notable navmesh impact.

![](/images/vanillaedit-oresandwildlife.png)