# Western District

![](/images/overview-westerndistrict.png)

## How It Came About

The original, initial idea of the mod is to extend Windhelm's southern walls straight west until it touches the mountains. The triangular space that would be carved out in the Tamriel worldspace does not contain anything interesting, and is a space other mods almost never touch. For a Windhelm expansion mod, this seems like a good way to claim a lot of space without introducing a lot of potential conflicts. This results in the Western District.

## Layout Concept

The old vanilla western walls that the expansion expanded past were nevertheless retained and redeveloped into the [High Road](highroad.md), which nicely partitions the city in an epic way and provides a clear visual boundary between the Western District and the vanilla areas. Despite that, it is easy for players and NPCs to cross the High Road over to the other side. From the Windhelm Market, you can directly walk through the underpass of the High Road bridge and enter the Western District. From Valunstrad, you can ascend stairs that goes onto the High Road, then descend stairs that goes down into the Western District on the other side.

The Western District's layout intends to be a contrasting experience. The High Road itself should be a bright, wide avenue with grand, open views that gives the player a sense of certainty, which should contrast with the dark, cozy alleyways underneath where you can disappear and get the comforting sense that the world is bigger than you. The [Meadhall](meadhall.md)'s bright and warm frontage contrasts with the private, cozy pathway to the [Windhelm Bathhouse](bathhouse.md) underneath the towering [Western District Temple](westerndistricttemple.md) that soars high into the sky and rises far above any other building in this side of Windhelm.

On the westernmost edge of the city, we make use of the cliffs by building a grand, majestic mansion into the mountainside. This resulted in [Windswept Manor](windsweptmanor.md), which now has its own backstory to explain its enormous size. Further past Windswept Manor, we take advantage of an existing protrusion in the rock cliffs by building stairs that climbs up to access the epic spectacle that is the [Beacon of Windhelm](beacon.md). In doing so, we also provide a more natural, remote backside of Windhelm that contrasts with the epic man-made structures in the main city.

## Individual Location Pages

* [Meadhall](meadhall.md)
* [Meadhall Apartments](meadhallapartments.md)
* [Western District Residences](westerndistrictresidences.md)
* [Western District Temple](westerndistricttemple.md)
* [Windhelm Bathhouse](bathhouse.md)
* [Windswept Manor](windsweptmanor.md)
* [Beacon of Windhelm](beacon.md)

Also see:

* [High Road](highroad.md)
* [Expanded Market](expandedmarket.md)
* [Southern Wallwalk](southernwallwalk.md)
* [Western Fort](westernfort.md)
