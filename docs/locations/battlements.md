# Battlements of Windhelm

EWE fully restores Windhelm's exterior walls, and adds battlements all the way around. This article is a listing of all that is involved.

## Layout and Logistics

To start, the walls of Windhelm is divided into 5 sections:

* [Southern Wallwalk](southernwallwalk.md)
* [Dockside](dockside.md)
* [Gray Quarter Wallwalk](grayquarterwallwalk.md)
* [Northeast Wallwalk](northeastwallwalk.md)
* [Northern Wallwalk](northernwallwalk.md)

There are 4 main garrisons for the guards that line the walls:

* [Western Fort](westernfort.md)
* [Dockside Keep](docksidekeep.md)
* [Northeast Castle](northeastcastle.md)
* [Mountain Caves](mountaincaves.md)

Specifically, soldiers of the Southern Wallwalk are stationed at the Western Fort. Soldiers of the Gray Quarter, Northeast, and Northern wallwalks are mostly stationed at the Northeast Castle. The Dockside Keep and Mountain Caves are lesser garrisons that contain shelter and supplies, but do not have leadership presence.

In addition to the main garrison structures (which are themselves strong defensive structures), there are additionally a number of towers along the walls. These include:

* [South Wall Tower](southwalltower.md)
* [Dockside South Tower](docksidesouthtower.md)
* [Dockside East Tower](docksideeasttower.md)
* [Eastern Tower](easterntower.md)

There are also two main gates that leads out the city, each fitted with a gatehouse.

* [South Gatehouse](southgatehouse.md)
* [Northeast Gatehouse](northeastgatehouse.md)

On the southern walls, there exists two catapult bastions.

* [Catapult Bastions](catapultbastions.md)

Lastly, the following are not part of the battlements, but nevertheless related because they occupy Windhelm's old vanilla walls:

* [High Road](highroad.md)
* [Windhelm Arena Market](arenamarket.md)

## Defensive Analysis

Disclaimer: I'm not an expert at all, but the following is fun to think about and fun to write, so here goes.

Windhelm is a fortress city, that when manned with enough loyal soldiers, should be nigh impossible to take by force.

At a glance, the city is surrounded by the White River which serves as a natural moat. It makes direct wall attacks from the south and southeast nigh impossible. In the southeast direction, the river literally hugs the sides of Windhelm's walls, and creates natural bottlenecks for any army that want to attack from that direction. Any ships that sail to the Windhelm Docks would face fierce fire from the towers above, and the assulting force would need to traverse the narrow passages that leads up to the Docks Gate, while getting death rained upon them from both sides above.

In the southern direction, there is indeed some open space between the walls and the river, however, the river still means that there would be no hope to ever push up a siege tower against the walls, thus a direct attack from the south could only be accomplished with exposed ladders. But to even get the ladders there, the enemy would need to cross the river efficiently in numbers while getting constantly rained upon by the arrows and catapult fire. Once crossed, the presence of ubiquitous machicolations as well as the numerous towers and bastions mean there is no safety anywhere for the attackers. Overall, such an assault is extremely difficult if not outright impossible. In bad weather, the attackers would be even more disadvantageous, for the defenders here have the roof covers above their heads protecting them from the elements.

Most likely, an enemy attacking from the south would need to assault the main gate. To do this, they need to first capture the heavily fortified Windhelm Bridge. Then they can attempt to assault the South Gatehouse. This would also not be an easy task. You may think of moving up a large battering ram, but if it does not get smashed to pieces by a catapult firing into its side, it would soon find that the large stone stairs in front of the gate makes it difficult to operate. Meanwhile, you would face not only arrows and projectiles, but also the pouring of hot sands or burning oil from the massive holes above the gate.

Perhaps one might consider constructing large siege weapons, or get some dwemer-crazy inventor to come up with strong cannons fueled by dwarven powder, and try to bombard the walls from afar to blast it open. But even if you could do that, you will still find Windhelm overwhelmingly formidable. Its walls are tall and thick, and can withstand enormous impact where the more flimsy walls of say, Whiterun or Riften, would crumble against. And even if you can knock down a section of the walls somehow, the resulting rubble would serve as yet another easily-defensible obstacle preventing you from storming the city from the opening.

The northeast direction is probably the easiest direction to assault Windhelm from. There we at least have an open area in front of the gates. This is less of a concern for the current Skyrim Civil War, for in that direction lies Winterhold, Windhelm's closest ally. Nevertheless, taking the city from that direction is perhaps _easier_, but it doesn't mean it's easy. You would be up against the massive Northeast Castle and the Northeast Gatehouse, as well as the platforms of the Eastern Tower, which together should still result in defenses greater than most other cities in Skyrim, save perhaps Markarth.

And if you do breach the Northeast Gatehouse, you will then find that you have only captured the lower portions of the Northeast District, which is itself surrounded by walls and partitioned off from the city proper. The stairs of the Windhelm Arena creates a tight bottleneck that you would need to fight your way up against endless defenders, before you can get into the city proper.

And throughout all this, the Beacon of Windhelm will burn an intense red, signaling to Windhelm's allies that Windhelm calls for aid. Thus, at any moment, know that the Whiterun or Riften could show up and batter at your sides. If you attack from the northeast, Winterhold could flank you from behind.

And should you, against all odds, breach the city proper, you will find that the defenders could easily retreat into the Mountain Caves, and establish yet another defensive position. From the caves, they could easily maintain a hold on the Beacon of Windhelm, the Northern Wallwalk, and the Northeast Castle, all being high vantage points where they can see your every move and attack you whenever the opportunity arises. In the worst case scenario, they still have an escape route by way of the Mountain Caves.

If you are the attacker, an open assault is not the answer. You must either breach it with spies, or starve it out with a prolonged siege. For a siege to work however, you will need to capture and maintain control of the Docks area, and also figuring out the back passage of the Mountain Caves and plug it up.
