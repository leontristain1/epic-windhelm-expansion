# Bank of Windhelm

![](/images/bank1.png)

The Bank of Windhelm is the first building you would pass by as you venture into the [Northern District](northerndistrict.md) from the [High Road](highroad.md). It is a simple, geometric building with a flat and imposing frontage but without any windows, and should give the sense that bankers are about the cold math of septims, laws, and contracts.

Our current lore of the [Windswept Manor](windsweptmanor.md) says the it is owned by the bank, so eventually, I hope to see the bank content and the Windswept Manor content being tied together somehow.

The bank could also have other potential playable content. For example, perhaps it allows the player to invest in various businesses and properties around the city. What if, say, you could invest in the [Meadhall](meadhall.md), and then help out the Meadhall owners with various quests, boosting their business, and in turn profit yourself?