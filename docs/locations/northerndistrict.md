# Northern District

![](/images/overview-northerndistrict.png)

## How It Came About

The initial idea of the mod is to extend Windhelm's southern walls straight west until it touches the mountains. In doing so, we would reclaim a triangular shaped space that became the [Western District](westerndistrict.md). We then realized that in addition to the triangular shaped space, there is also a sizable area _behind_ the city we can expand into. It would be enclosed by the northern mountains and the Palace of the Kings. This resulted in the Northern District.

## Layout Concept

The Northern District is located in perhaps the most enclosed location of all of Windhelm. From the South Gate, it exists at the very far back of the city under the heights of the Palace of the Kings. Being itself located on a higher elevation than the rest of city, here we have the safest and most well-protected area of Windhelm.

Being so safe and well-protected, you might think this to be a high-class district, except the hardy folks of Windhelm would likely scoff at the idea that anywhere in Windhelm would be considered high class, especially compared to the extravagance in Solitude. Also worth noting is that the height of the Valunstrad houses means that despite the high elevation, the views don't go as far as you might think.

The greatest spectacle of this corner of the city is the [Northern District Temple](northerndistricttemple.md), which is by far the most grandiose of all Windhelm temples. The [Stonemasonry Guild](stonemasonryguild.md) is located in its lopsided wing, and is what keeps the ancient traditions of Windhelm's stoneworking alive.

You'll also find the [Bank of Windhelm](bank.md), as well as a large, well-known [Northern District Mansion](northerndistrictmansion.md) that comes with its own servant's quarters, plus a few more residence buildings. All of this surrounds a [Norther District Plaza](northerndistrictplaza.md) complete with a Ysgramor statue and some market stalls, and with the Palace of the Kings in the backdrop.

Also close by is the [Mountain Gate](mountaingate.md) which leads into the enormous mountain caves - a hidden but nevertheless integrated part of Windhelm's city. The caves goes far beyond into the mountain, full of unknown depths that have not been mapped out for ages and thus belongs to the domain of stories and legends.

## Individual Location Pages

* [Mountain Gate](mountaingate.md)
* [Bank of Windhelm](bank.md)
* [Stonemasonry Guild](stonemasonryguild.md)
* [Northern District Residences](northerndistrictresidences.md)
* [Northern District Mansion](northerndistrictmansion.md)
* [Northern District Plaza](northerndistrictplaza.md)

Also See:

* [High Road](highroad.md)
* [Northern Wallwalk](northernwallwalk.md)
