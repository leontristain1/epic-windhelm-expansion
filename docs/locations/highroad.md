# High Road

![](/images/highroad1.png)

The High Road is an elevated avenue that runs along the old vanilla Western Walls of Windhelm, now a structure that runs down the middle of the city, separating the [Western District](westerndistrict.md) from the vanilla city areas. It is directly next to the Hall of the Dead (graveyard), House of Clan Shatter-Shield, Hjerim, [Meadhall](meadhall.md), and [Western District Temple](westerndistricttemple.md).

In the southern end, it passes beside the [Expanded Market](expandedmarket.md) over a bridge and connects to the [Southern Wallwalk](southernwallwalk.md). In the northern end, it can circle over to descend to the alleys next to the [Windhelm Bathhouse](bathhouse.md), or ascend further up to the [Mountain Gate](mountaingate.md) and the [Northern District](northerndistrict.md).

The High Road is wide and open. There is nothing to obstruct your view up here. The sky is huge and you can see far in many directions. It runs right next to the Valunstrad area, and its sides are lined with Valunstrad fences. In the lore, "Valunstrad" has the meaning of "Avenue of Valor". If we twist the lore a bit, perhaps we can relabel the High Road as the real "Avenue of Valor", for it is certainly more grand compared to the windy, angular little roads in Valunstrad itself.

