# Gray Quarter Wallwalk

![](/images/grayquarterwallwalk1.png)

The Gray Quarter Wallwalk are the wall-level areas surrounding the Gray Quarter. This stretch of walls lies between and connects together the [Northeast Wallwalk](northeastwallwalk.md) and the [Dockside](dockside.md) area.

The Gray Quarter Wallwalk is a two-tiered structure. There is a higher level that hugs the battlements, where the [Dockside East Tower](docksideeasttower.md) is located on its southern end. There is also a lower level where two doorways that leads to what is known as the "Gray Quarter Wall Storerooms" are found.

The Gray Quarter Wallwalk also includes the stretch of walls on the Gray Quarter side of the dock path that leads to the dock gates. Here, the Gray Quarter Wallwalk ascends up, passing just above the dock gate, and connects to the Dockside Keep and the Stone Quarter.

From the wallwalk level, there are no direct passages down to the Gray Quarter below. Instead, one would have to either go the long way around and circle back from the Stone Quarter. This is intentional, for lore-wise it makes more sense for the Gray Quarter proper to be isolated within the city.

You may notice a line of metallic gutter along the ground in front of the two store rooms here. This gutter is added in anticipation to possible eventual interior expansion of the Gray Quarter. The idea is that, we can maybe add a door somewhere in the Gray Quarter that leads into an interior common space just underneath this gutter. The gutter would provide that space with natural light during the day, but also further add to the "lowest class" wretched feeling of the Gray Quarter. Then, from that common space, we can branch to more interior "rooms" that serve as the homes of new NPCs, similar to how [Windswept Manor Commons](windsweptmanorcommons.md) works.