# Northeast District Temple

![](/images/arenamarket8.png)

The Northeast District Temple is located in the [Northeast District](northerndistrict.md), with its entrance opening right into the lower section of the [Windhelm Arena Market](arenamarket.md). When descending into the Northeast District, this is the first proper Northeast District building that you will see.

The temple is the main location of worship for the residents of the Northeast District. After all, the Temple of Talos is pretty far away from here, and the [Western District Temple](westerndistricttemple.md) is all the way on the other side of the city.

Across an alleyway from the temple are the [Northeast District Residences](northeastdistrictresidences.md). Down the alleyway, the [Eastern Tower](easterntower.md) can be seen. The temple is a long building, and has a back part known as [Northeast District Temple Back](northeastdistricttempleback.md), which is a different interior for a different purpose.