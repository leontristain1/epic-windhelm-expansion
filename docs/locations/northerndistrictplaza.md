# Northern District Plaza

![](/images/northerndistrict1.png)

The various structures of the [Northern District](northerndistrict.md) all surrounds an open plaza area, with the centerpiece being a statue of Ysgramor, and market stands beside.

At the end of the plaza, next to the Palace of the Kings, is also a statue of Talos, and from there, stairs can lead down into the Palace of the Kings courtyard. On the southern edges of the plaza, there is a stretch where you can look down at Valunstrad through the fences.