# Windhelm Bakery

![](/images/bakery1.png)

The [Expanded Market](expandedmarket.md) provided more space in the market square. In this additional space, we add a new wooden building that blends nicely with The White Phial building. This is the Windhelm Bakery building. Outside, you can find Hillevi Cruel-Sea's produce stand.

Despite the building currently considered the "Bakery" building, it is worth noting that it provides 3 additional doors in its ground level. Out of these, perhaps only the rightmost door will lead to the actual bakery itself. The innermost door is the most difficult to get to, and is awkward to use as a major door. Most likely it will lead to some "storage space" that won't attract much traffic.

The Bakery building is actually directly connected to the [Meadhall](meadhall.md)'s underbridge section. Thus, the left door perhaps can be an alternate entrance to the Meadhall.

If we have a bakery, perhaps it could provide baked goods delivery service to the [Five Taverns of Windhelm](fivetaverns.md), adding to the city's foot traffic, life, and activities.
