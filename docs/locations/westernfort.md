# Western Fort

![](/images/westernfort2.png)

At the western end of the [Southern Walls](southernwallwalk.md) is the Western Fort. This defensive structure marks the westernmost point of the city, and serves as the stronghold that anchors the wall into the mountain. From its roofs, city guards have full visibility for threats coming from the direction of Anga's Mill, as well as any shenanigans that may happen on the westward road. The Western Fort is where the Southern Wallwalk soldiers are garrisoned.