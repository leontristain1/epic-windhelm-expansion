# Mountain Caves

NOTE: This is planned content.

Currently, EWE provides several entrances that leads into the mountain cliffs that borders Windhelm's northwest. The biggest - the [Mountain Gate](mountaingate.md) - is supposed to be the main entrance into the caves. There are also smaller entrances from the [Beacon of Windhelm](beacon.md), and implied interior entrances via the [Northern District Temple](northerndistricttemple.md) and the [Western District Temple](westerndistricttemple.md).

The Mountain Caves intend to play a role inspired by what [Glittering Caves](http://tolkiengateway.net/wiki/Glittering_Caves) is to [Helm's Deep](http://tolkiengateway.net/wiki/Helm%27s_Deep) - not so much in the "marvelous" aspect, and more so in its role in defense. It's a place where if Windhelm is taken, defenders could retreat into it and easily defend it from the outside, and if things get even worse, it could be an escape route.

The caverns would be extraordinarily large and open, though perhaps not to such civilization heights where they would be an eye opener and no mistake (there is such a thing as _too much_ Windhelm, I found - let's leave that for a Markarth expansion idea). The beginning areas would be part of a Windhelm guard garrison where it is safe and well watched. Activities here could be considered normal everyday part of the city. A few side caves would be used for guard quarters in order to join with the Northern Wallwalk.

But if you go deeper, guard presence will end at some point, and the areas will be considered dangerous. The deepest parts of the caverns may not have been mapped for centuries. There could be stories of people who venture too deep and would never come back. In the greatest depth, maybe it can even connect into the southeastern edges of [Blackreach](https://en.uesp.net/wiki/Skyrim:Blackreach).

There would be one known passageway, that though not patrolled, is at least known to guards. The passageway would end up at a locked metal gate that leads to a small cave that exits out somewhere beyond the [Northern Mountains](northernmountains.md). This is the escape route should Windhelm ever be taken.

![](/images/mountaingate1.png)
![](/images/beacon1.png)
![](/images/northernwallwalk2.png)