# Northeast District Residences

![](/images/northeastdistrictresidences1.png)

In the [Northeast District](northeastdistrict.md), there are 6 buildings that are together referred to as the Northeast District Residences. Two are [Rochester](https://www.nexusmods.com/skyrimspecialedition/mods/28758)-style buildings on the left of the alley to the [Northeast District Pub](pub.md), and the remaining four are [Stormcloak Cabin](https://www.nexusmods.com/skyrim/mods/66514/)-style buildings located in the middle of the district.

These are where the ordinary people of Northeast Windhelm live. Some could be shops.