# Northeast District

![](/images/overview-northeastdistrict.png)

## How It Came About

The Northeast District was the necessary result of two goals - the [Windhelm Arena Structural Restoration](arena.md) and the [Northeast Gate](northeastgatehouse.md). Both goals were important. After all, the greatest vision for an expanded Windhelm must absolutely include a restored Windhelm Arena, and a gate that opens to the northeast for a perfectly sensible and straightforward road to Winterhold, its closest ally in the Civil War.

However, the Arena's vanilla footprint would've strangely put it half outside the city. A northeast gate should also not be placed in the Gray Quarter, since back when the Dunmer fled to Windhelm when the Red Mountain erupted, the Jarl would certainly not have chosen to let them live in any strategically important part of the city. Lore-wise, it makes more sense to keep the Gray Quarter a closed loop without strategic importance.

Therefore, we need a new district in the northeast, both to keep the Arena's footprint within the walls, and to provide a gate that does not go straight into the Gray Quarter. This resulted in the Northeast District.

## Layout Concept

The [Northeast Walls](northeastwallwalk.md) are made to be diagonal in order to keep its footprint restrained and yet still provide enough space for a legitimate district. The diagonal stretch also coincidentally made it somewhat matching Windhelm's outline in Elder Scrolls Online, kind of.

To naturally connect the diagonal wall with the Gray Quarter wall corner, I made use of the `whouterwallc1` model, resulting in the protrusion in the wall profile which became the [Eastern Tower](easterntower.md). The [Northeast Gate](northeastgatehouse.md) is then placed in the wall spot where a road to Winterhold can head straight out into the distance. Then, where the walls met the mountain, we have the [Northeast Castle](northeastcastle.md) serving as the anchor.

The Gray Quarter need to remain a closed loop, both for lore reasons, and because the Gray Quarter meshes are rather inflexible and difficult to change. This meant a passage to the Northeast District is not possible at the ground level for the entirety of the Gray Quarter wall. However, the structural restoration of the [Windhelm Arena](arena.md) conveniently replaces the old prison building's `L`-shaped passageway (between Aretino Residence and Palace of the Kings courtyard) with a new building that has a `+` shaped crossroads passageway, allowing direct access to the Gray Quarter Wallwalk from the Aretino Residence and Palace of the Kings. Thus, the Arena corner is where we can have descending stairs to access to the Northeast District. This makes it easy to get to the Northeast District while nicely bypassing Gray Quarter.

Following the natural landscape, the Northeast District would slope downward, making it the lowest elevation district in the city. The placement of [Northeast District Temple](northeastdistricttemple.md) was an attempt to not only provide a temple building, but also to cover up the walls that separated it from the Gray Quarter, which would've looked ridiculously high from the low elevation.

With the epic structures of Northeast District Temple, Northeast Castle, Northeast Gatehouse, and Eastern Tower on all sides, the remaining structures of the Northeast District need to be mundane, and also placed in the center because the sides are all taken. This resulted in the [Northeast District Residences](northeastdistrictresidences.md) buildings which provides alleyways. One alleyway would feature the [Northeast District Pub](pub.md) at the end, while another would run beside the temple and feature the Eastern Tower down its narrow sight.

The Eastern Tower provides a spiral staircase in the center that allows players and NPCs to climb the walls without a loading door. However, this should not be relied upon heavily by NPCs because the spiral stair passage is extremely narrow, and while one NPC could successfully traverse it, two would certain get stuck in opposite directions. There are also ways to climb up to the walls through loading doors via the gatehouse and castle's interiors. From the castle, one can further exit out to the west and onto the [Northern Wallwalk](northernwallwalk.md) and eventually access the [Mountain Caves](mountaincaves.md) at the other end.

## Individual Location Pages

* [Arena Market Shops](arenamarketshops.md)
* [Northeast District Temple](northeastdistricttemple.md)
* [Northeast District Temple Back](northeastdistricttempleback.md)
* [Northeast District Pub](pub.md)
* [Northeast District Residences](northeastdistrictresidences.md)
* [Northeast District Warehouse](northeastdistrictwarehouse.md)
* [Northeast Gatehouse](northeastgatehouse.md)
* [Northeast Castle](northeastcastle.md)

See Also:

* [Windhelm Arena](arena.md)
* [Windhelm Arena Market](arenamarket.md)
* [Northeast Castle Back Plaza](northeastcastlebackplaza.md)
* [Northeast Castle Side Room](northeastcastlesideroom.md)
* [Northeast Wallwalk](northeastwallwalk.md)
* [Northern Wallwalk](northernwallwalk.md)
