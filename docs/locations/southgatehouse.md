# South Gatehouse

![](/images/southgatehouse5.png)

The South Gatehouse is the structure that defends Windhelm's primary city gate. It is composed of two towers linked together by a middle platform that goes above the gate itself.

The two gate towers can be accessed from the [Southern Wallwalk](southernwallwalk.md) level. Each tower has an interior that further allows access to the tower's roof, as well as a side door that leads to the middle platform. The middle platform is an exterior area with two large grates on the floor, where during a siege, hot oils and sands can be poured down to be rained upon any battering rams attacking the gate.