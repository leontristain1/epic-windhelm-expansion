# Mountain Gate

![](/images/mountaingate1.png)

The Mountain Gate is a massive gate that leads into the [Mountain Caves](mountaincaves.md). It is located at the end of the [High Road](highroad.md), in-between the [Western District](westerndistrict.md) and the [Northern District](northerndistrict.md). It itself is considered part of the Northern District.