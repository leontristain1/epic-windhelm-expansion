# Windswept Manor Subdivisions

![](/images/windsweptmanorinterior3.png)

As explained in the [Windswept Manor](windsweptmanor.md) backstory, the manor is currently owned by the bank and maintained through a common fund. What used to be a ridiculously large building has since been subdivided. Each subdivision is either owned or rented by folks who collectively make up the residents of Windswept Manor.

The subdivisions have doors that open into the [Windswept Manor Commons](windsweptmanorcommons.md). Some subdivisions, such as the two wings, are quiet and roomier, while other subdivisions could have doors that open right into the tavern area of the commons where folks could be rowdy after a hard day and a few bottles of mead.

![](/images/windsweptmanorinterior4.png)