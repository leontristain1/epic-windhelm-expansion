# Dockside East Tower

![](/images/dockside2.png)

The Dockside East Tower is located at the southern end of the [Gray Quarter Wallwalk](grayquarterwallwalk.md). It is a half-tower that overlooks the Windhelm Docks, with stairs leading up to its roof from the outside, as well as a wall-level doorway that leads to a storage-like room within. A corner of the tower is damaged, perhaps in some past battle.
