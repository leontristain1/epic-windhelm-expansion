# Catapult Bastions

![](/images/southernwallwalk10.png)

There are two catapult bastions along the [Southern Wallwalk](southernwallwalk.md). These are wall-level bastions with open space, where the catapults can be rolled to aim during a battle (in imagined lore, not actually game content).

The player can fire the catapult for fun, though the guards might not like the player wasting ammunition and endangering potential innocent travellers in the distance.