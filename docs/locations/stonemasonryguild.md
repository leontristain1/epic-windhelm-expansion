# Stonemasonry Guild

![](/images/stonemasonryguild1.png)

The Stonemasonry Guild is located in-between the [Northern District Temple](northerndistricttemple.md) and the [Northern District Mansion](northerndistrictmansion.md).

Windhelm is an ancient city with massive stone walls, and just about everything within the city is built out of stone. So certainly, the city has a long, ancient stonemasonry tradition, and that tradition is kept alive by the Stonemasonry Guild. The guild members mostly live within their guild house and form a close-knit fraternity.

Every morning, they would file out of the guild house and walk to various parts of the city and work on the stone. As you walk around Windhelm, you will probably hear the familiar repeated sounds of hammer bashing at stone - that is the guild members working. Especially in the Stone Quarter, there's a lot of broken stone to be fixed, and their presence would add traffic and therefore liveliness to the roads. They can even come with enigmatic emblems and complex rituals that inspire conspiracy theories.
