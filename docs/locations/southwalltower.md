# South Wall Tower

![](/images/southernwallwalk11.png)

The South Wall Tower is tower on the [Southern Wallwalk](southernwallwalk.md) midway between the [Western Fort](westernfort.md) and the [South Gatehouse](southgatehouse.md). It is located right next to the [High Road](highroad.md)'s bridge.

The tower has a small warehouse-like interior space, accessible from the wallwalks. From the inside, you can further climb up to the roof.