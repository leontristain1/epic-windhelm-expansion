# Northern District Temple

![](/images/northerndistricttemple2.png)

The [Northern District](northerndistrict.md) hosts Windhelm's largest temple. This is a retextured and slightly hacked version of the Sovngarde meadhall building from vanilla Skyrim. Its presence towers high above and forms one of the most spectacular architecture of the expanded Windhelm.

In front of it is the [Northern District Plaza](northerndistrictplaza.md). The [Bank of Windhelm](bank.md) is to its left, and the [Stonemasonry Guild](stonemasonryguild.md) is to its right. The interior of the temple is built into the mountain, and has a direct connection to the [Mountain Caves](mountaincaves.md). It also serves as the western terminus of the [Northern Wallwalk](northernwallwalk.md).

If Solitude has the Temple of the Divines, then perhaps this can be the Temple of the _Nine_ Divines, which is Windhelm's answer.