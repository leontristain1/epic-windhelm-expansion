# Meadhall Apartments

![](/images/meadhallapartments1.png)

The backside of the [Meadhall](meadhall.md) are three rather modest living spaces. Each of them are two-level loft-like units with thin and short interiors.

Folks living here probably don't have a lot of means. Either that, or they prefer the simple life. Living here means giving up space and luxuries for the convenience of living right next to the [Meadhall](meadhall.md), [Western District Temple](westerndistricttemple.md), [Windhelm Bathhouse](bathhouse.md), and the shops of [Western District Residences](westerndistrictresidences.md). The [Expanded Market](expandedmarket.md) is also within easy walking distance. Here, you don't have much, but the neighborhood is safe and lively, and you rather enjoy that.