# Dockside South Tower

![](/images/docksidesouthtower1.png)

The Dockside South Tower is a tower that marks the transition point between the [Southern Wallwalk](southernwallwalk.md) and the [Dockside](dockside.md) area. It is a half-tower, where its top platform is accessed from the outside via stairs behind from the Southern Wallwalk. It has a storage-like room within that can be accessed from a side door on the Dockside level.