# Dockside

![](/images/dockside1.png)

The Dockside area is the large open space on top of the flat roofs of the Docks structure that houses the East Empire Company, the Argonian Assemblage, and the Clan Shatter-Shield Office from the vanilla game. In EWE, the roofs of the Docks structure is now paved and lined with battlements and treadmill cranes, and is now referred to as the Dockside area.

The most prominent structure located in the dockside area is the [Dockside Keep](docksidekeep.md), which stands tall beside the passageway that leads to the dock gate. Otherwise, the area is a large open space primarily for guard activities. When new recruits are being trained, it tend to happen somewhere between the Dockside and the [Gray Quarter Wallwalk](grayquarterwallwalk.md), where store rooms are conveniently close by. On the northern part of Dockside, there are currently some archery targets for guards to practice with.

The treadmill cranes are placed such that their platforms are each atop an open space in the docks below, where one can imagine them being lowered each morning, where fishermen at the docks would load it with buckets of freshly caught delicacies to then be brought up, and carried over to the [Arena Market](arenamarket.md). The cranes pose a work hazard to the dock workers below, adding to the shenanigans the Argonians have to put up with when living here.

There are two wall-towers associated with Dockside - the [Dockside South Tower](docksidesouthtower.md) and the [Dockside East Tower](docksideeasttower.md). Neither are truly in Dockside proper, but exists right next to it. Specificaly, the Dockside South Tower marks the eastern end of the [Southern Wallwalk](southernwallwalk.md) where it descends down to Dockside via stairs. The Dockside East Tower, likewise, is actually located over on the Gray Quarter Wallwalk. Both towers -  but especially the Dockside East Tower - have enough line of sight to keep a watchful eye on the Windhelm Docks below.

![](/images/dockside3.png)