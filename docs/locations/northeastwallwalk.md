# Northeast Wallwalk

![](/images/northeastwallwalk1.png)

The Northeast Wallwalk is the stretch of walls between the the western end of the [Windhelm Arena Market](arenamarket.md) and the [Northeast Castle](northeastcastle.md). It encircles the [Northeast District](northeastdistrict.md). Along its length is the [Eastern Tower](easterntower.md), the [Northeast Gatehouse](northeastgatehouse.md), and of course, the Northeast Castle at the end. To its south, it connects directly to the [Gray Quarter Wallwalk](grayquarterwallwalk.md).

![](/images/northeastwallwalk2.png)