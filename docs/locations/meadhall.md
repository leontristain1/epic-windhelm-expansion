# The Meadhall

![](/images/meadhall1.png)

The Meadhall is a tavern building located on the other side of the [High Road](highroad.md) bridge underpass from the [Expanded Market](expandedmarket.md). It is the first building you see when venturing into the [Western District](westerndistrict.md). When the market activities dies down at dusk, folks finishing up a day's hard work would file into the Meadhall for drinks and merriment.

The Meadhall in its current form is made up of two structures - the "Old Building" and the "Underbridge". The old building is the left section with large shingles and smokestack, and barrels of mead in front. The Underbridge is the structure built directly into the [High Road](highroad.md)'s bridge.

One can imagine that the Underbridge is where folks make merry today. The floor level is where folks eat and drink, while its second floor hold inn rooms, that can lead to an inn reception at the [Windhelm Bakery Building](bakery.md) alternate entrance. That's right, it can all be connected on the inside. Whereas, perhaps the old building is where the owner family lives, and it could even have room to host an Adventurer's guild of some kind.

The Meadhall has its own rather unique mead-making process. The folks here take advantage of Windhelm's always-snowing climate to freeze-distill their mead in massive quantities, resulting in a much stronger and more concentrated mead. You can see the process in action with the giant mead barrels outside directly in the snow. Those are only for display, however. There are many more barrels in production in the basement of the underbridge.

For some reason, despite the quality mead, it has yet to compete directly with Black-Briar and Honningbrew Mead. Perhaps the owner is not business-savvy enough to set up supply chains across Tamriel and is waiting for your help, perhaps folks in other cities aren't hardcore enough to enjoy such a strong mead and prefer more milk-like varieties, or perhaps the civil war has disrupted the city's ability to compete abroad. Nevertheless, for the time being, the mead here is a local delicacy waiting for the player to stumble upon.
