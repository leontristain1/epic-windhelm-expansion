# Northern Wallwalk

![](/images/northernwallwalk1.png)

The Northern Walls of Windhelm is a long piece of wall that extends between the [Northeast Castle](northeastcastle.md) and the [Northern District Temple](northerndistricttemple.md). The walls create a formal northern edge for the city proper, even though shortly past the walls is the near-vertical cliffs of the [Northern Mountains](northernmountains.md) that make it impossible for any enemy army to attack from that direction.

Nevertheless, there could be occasional spies that descend the walls to try to eavesdrop on important information, though given Windhelm's normally wide open city gates, why they didn't simply disguise themselves as ordinary travellers is anyone's guess. To look out for these kinds of intruders, the Northern Wallwalk is nevertheless patrolled on a regular basis.

![](/images/northernwallwalk2.png)