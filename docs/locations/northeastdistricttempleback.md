# Northeast District Temple Back

![](/images/northeastdistrict2.png)

The back half of the [Northeast District Temple](northeastdistricttemple.md) building has a facade of solid stone, with a door at the corner located just next to the wooden stairs that goes up to the [Eastern Tower](easterntower.md). Inside this door is a space that I am currently calling the "Northeast District Temple Back" (for lack of a better idea).

This building really could serve any purpose, such as a school, a library, a museum, or some kind of shop. The specifics will probably be decided later on once the district gains some activity. Perhaps then I can get a feel for what is missing.