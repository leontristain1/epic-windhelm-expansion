# Windswept Manor

![](/images/windsweptmanor1.png)

Located on the western edge of the city, Windswept Manor is a massive house built into the mountains. Most people's first impression to such a house is how anyone could have built any house so ridiculously massive.

Windswept Manor was first built, many ages ago, by a foolish thane who wanted a home larger than the Jarl's. He tooked out massive loans against his business investments in the Snow Quarter in order to pay for it. Many thought it a ridiculous idea. The Jarl rolled his eyes at it. But when it was finally complete, indeed, it was an epic sight to behold.

But then, the Red Mountain erupted and the Dunmer refugees started arriving. As wretched makeshift camps grew rampant outside the city walls, the Jarl decided to confiscate the Snow Quarter - the least strategically important of all Windhelm quarters - in order to provide those poor but hard-to-trust strange outsiders a home. The Jarl gave the thane some compensation, but times were tough and the city's coffers were strained as it is, which meant the compensation was similarly thin. Suddenly, the thane found himself in total financial collapse.

In subsequent years, the bank forced him to sell off his lavish manor to pay off his debts. Since then, Windswept Manor has been owned by the bank and maintained through a common fund, with its interiors subdivided into multiple individual units to be rented or sold. Today, the insides of Windswept Manor houses a sizable population that makes it almost its own district.

Today, the great hall at the ground floor, known as the [Windswept Manor Commons](windsweptmanorcommons.md), is public space. Residents live in various [Windswept Manor Subdivisions](windsweptmanorsubdivisions.md). There is also a [Windswept Manor Kitchen](windsweptmanorkitchen.md) where the manor cook prepares the meals and drinks that are served in the commons.

The upper half of the manor, known as [Windswept Manor Suite](windsweptmanorsuite.md), is currently unoccupied. It is owned by the bank, and for some reason, the bank is not willing to sell or rent it out. People call it a shame, because this is the most coveted subdivision of the whole manor, and has access to the massive balcony that provides beautiful views of the rest of Windhelm. Maybe with some courage, wits, and investigative abilities, the player can acquire this property as his or her Windhelm home.

There is also a very large, open space in the basement, that can be accessed through the large gate on the bottom of the structure. We imagine this space to house a workshop of some sort - perhaps a siege workshop where the catapults are made. The interior should be full vaulted ceilings with solid stone in order to prop up all the weight above convincingly.