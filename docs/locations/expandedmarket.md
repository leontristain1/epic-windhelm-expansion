# Expanded Market

![](/images/expandedmarket2.png)

In the vanilla game, the Windhelm Market is a dead end. The road next to the southern walls would feed travellers into the market. Once in the market, the only other way out is through the north exit which leads to the Hall of the Dead. The vanilla market is also rather small and cramped.

In the expanded Windhelm, the market square has been expanded further to the west by roughly a third the original size. In the above image, the exact line where the vanilla market ends would be the vertical line that would align with the two vertically-placed market stalls on the left. Everything left of it are additional space. The market is also no longer a dead end. The road next to the southern walls now goes through, underneath the [High Road](highroad.md)'s bridge, and onto the [Western District](westerndistrict.md).

In the newly expanded corner, we add a new [Windhelm Bakery Building](bakery.md) structure, which can house a bakery inside, as well as an alternate entrance to the [Meadhall](meadhall.md).

The market stalls in the vanilla game were placed based on the market being a dead end. Now that the market is a throughfare, the original locations of some of the market stalls are no longer appropriate. Thus, several of the market stands have been relocated to more sensible locations for the new space.

One other notable thing to mention is that wallwalk stairs have been added to the market square. A consequence of this is that the [Blacksmith Quarters Attic](blacksmithquartersattic.md) is now directly accessible as its own new interior.