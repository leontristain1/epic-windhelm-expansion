# Blacksmith Quarters Attic

![](/images/blacksmithattic1.png)

Restoring the [Southern Wallwalk](southernwallwalk.md) provided the opportunity to turn the attic of the Blacksmith Quarters into its own individual interior.

This attic space can be used for various purposes. An obvious idea is to turn it into a player home similar in style to [Astronomer's Loft](https://www.nexusmods.com/skyrimspecialedition/mods/38059). Another idea is to turn it into a home for Hermir Strong-Heart - Oengul's apprentice. It feels odd to me that the two of them, being master-apprentice in relationship, would sleep next to each other like a married couple in the vanilla game.

An implementation concern to keep in mind here is that from the normal Blacksmith Quarters interior, if you look up, you will see the vaulted ceiling. Therefore, to be perfectly immersive, if the attic is to be its own separate space, then floor boards should probably be added into the Blacksmith Quarters, making the roof flat instead.