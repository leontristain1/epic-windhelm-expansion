# Windswept Manor Suite

![](/images/windsweptmanorinterior5.png)

The top portion of [Windswept Manor](windsweptmanor.md) is the largest and most coveted of [Windswept Manor Subdivisions](windsweptmanorsubdivision.md). Its door is located up a flight of stairs at the end of [Windswept Manor Commons](windsweptmanorcommons.md) next to the fireplace.

For one reason or another, the Bank refuses to sell or rent it. Perhaps a witty and courageous player with investigative abilities could uncover the reason and resolve the issue, and in doing so make the top suite his or her Windhelm home. If you do this, you will be able to enjoy the large, expansive balcony with beautiful views.

![](/images/windsweptmanor2.png)
![](/images/windsweptmanor4.png)