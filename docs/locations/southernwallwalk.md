# Southern Wallwalk

![](/images/southernwallwalk8.png)

The Southern Wall is the longest stretch of Windhelm's exterior walls. It extends from the [Western Fort](westernfort.md) all the way east to the stairs leading down to the [Dockside](dockside.md). Soldiers assigned to this portion of the walls operate out of the Western Fort.

Along the wall you will find two [Catapult Bastions](catapultbastions.md), the [South Wall Tower](southwalltower.md), the [South Gatehouse](southgatehouse.md), and the [Dockside South Tower](docksidesouthtower.md). Windhelm's primary city gate is along this wall at the South Gatehouse. The wall also directly connects to the [High Road](highroad.md) over a bridge, and has wallside stairs where it can be accessed from the Blacksmith Quarters, the [Expanded Market](expandedmarket.md), and the western end of the [Western District](westerndistrict.md).

Along the Southern Wallwalk, you will find wooden coverings that protect defenders from the frequent blizzards Windhelm tend to experience. When you're pelting arrows at your enemies down those machicolations, it would be nice to keep the wind and snow away from your face, and to be next to a nice, warm fire.

![](/images/southernwallwalk6.png)