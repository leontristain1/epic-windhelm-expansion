# Windswept Manor Kitchen

![](/images/windsweptmanorinterior2.png)

The food and drinks served in the [Windswept Manor Commons](windsweptmanorcommons.md) are prepared in the kitchen, located behind a door at the end of the hall next to the fireplace. A resident cook hired through the common fund is responsible for manning the kitchen.