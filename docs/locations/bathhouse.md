# Windhelm Bathhouse

![](/images/bathhouse1.png)

Tucked far below and underneath the massive [Western District Temple](westerndistricttemple.md) is a small, cozy alleyway where you can find the Windhelm Bathhouse. Despite the outer facade, the interior is built into the mountains, and integrated well with the natural caverns and hotspring that lies underneath.

Folks that work in the [Expanded Market](expandedmarket.md) and drink at the [Meadhall](meadhall.md) could walk down the alley into the bathhouse for a nice warm bath, before heading back to their [Western District Residences](westerndistrictresidences.md) and [Meadhall Apartments](meadhallapartments.md) to retire for the night.

When this space is eventually built out, we will have pipes that exhaust steam into the alleyway, further providing the idea of warmth and coziness.

![](/images/westerndistrictalleys3.png)