# Windhelm Arena Market

![](/images/arenamarket1.png)

The Windhelm Arena Market is the primary market on the east side of Windhelm. It occupies the top of vanilla Windhelm's northern walls, which now exists as an internal wall barrier that separates the Gray Quarter and the [Northeast District](northeastdistrict.md).

On the west side, next to a Talos statue, the market connects to the [Windhelm Arena](arena.md)'s prison structure, which provides direct access to the Palace of the Kings and the Stone Quarter via the Aretino Residence. Also next to the Talos statue are the main stone stairs passage down to the Northeast District, as well as the road that leads north to the [Northeast Castle](northeastcastle.md)'s [Back Plaza](northeastcastlebackplaza.md). Overall, the western end of the Arena Market is a crucial transportation junction, providing the market the customer foot traffic it needs to stay vibrant.

On the east side, the market connects directly to the [Gray Quarter Wallwalk](grayquarterwallwalk.md) and the [Eastern Tower](easterntower.md), and from there to the [Dockside](dockside.md) where treadmill cranes would bring up buckets of fish every day from the docks.

![](/images/arenamarket4.png)

The Arena Market is a fully covered market in epic fashion, with roofs that protects the market activities from the elements outside, so that even on the stormiest of days, the market remain cozy enough that people can come visit. The sides of the market are lined with stalls, but there are also windows of space where you can approach the edge and look down at the Gray Quarter below.

The existence of the market also adds an extra layer to Gray Quarter lore. Not only can you now see the epic market high above when down in the gutters of the Gray Quarter, but you can also imagine the various juices of rotting vegetables seeping over the edge and flowing down the walls, accumulating onto the roofs of Gray Quarter structures. Anbarys Rendar's line of *"All the filth from the upper quarters flow downhill"* now takes on a more intense meaning.

![](/images/arenamarket14.png)

The Arena Market also has a lower area on the bottom of the stairs that leads down to the [Northeast District](northeastdistrict.md). This is a small square in front of the [Northeast District Temple](northeastdistricttemple.md). Here, we have a few stalls and a few shop doors that make up the [Arena Market Shops](arenamarketshops.md).

![](/images/arenamarket9.png)