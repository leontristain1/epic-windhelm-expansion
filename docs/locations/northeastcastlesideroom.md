# Northeast Castle Side Room

![](/images/northeastdistrictcastle7.png)

The [Northeast Castle](northeastcastle.md) has a room on its side that is fairly small and a bit detached from the castle proper. The room has a fairly large and elaborate door (compared to regular city doors anyway). This room is known as the Northeast Castle Side Room. A purpose has not yet been decided for this room.