# Northeast Castle Back Plaza

![](/images/backplaza2.png)

The Northeast Castle Back Plaza is a courtyard-like space at the foot of the [Northeast Castle](northeastcastle.md), directly to its west. It is nestled between the [Windhelm Arena](arena.md), the Palace of the Kings, and the [Northern Walls](northernwallwalk.md). The plaza ascends up to a Statue of Talos at the very end.

Also to the left side of the plaza is a new door that leads into the Arena/Palace structure. This door has been referred to as the "Northeast District Arena Back". Perhaps it could lead to a new space associated with the Palace of the Kings. Or it could be a mini-temple, a mini-museum, or some isolated shop that few people visit.

![](/images/arenaback1.png)