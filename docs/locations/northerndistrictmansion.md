# Northern District Mansion

![](/images/northerndistrictmansion1.png)

The Northern District Mansion is a large house in the [Northern District](northerndistrict.md) that comes with its own courtyard and a side door into a servants quarters. It is the innermost building of the Northern District, and located right next to the Palace of the Kings.

This place could be the home of an influential thane that comes from one of the ancient clans of Windhelm.