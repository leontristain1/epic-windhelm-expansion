# Northeast District Warehouse

![](/images/northeastdistrictwarehouse1.png)

Yet another [Stormcloak Cabin](https://www.nexusmods.com/skyrim/mods/66514/)-style building is pushed into the [Northeast Walls](northeastwallwalk.md), forming a structure that looks more fitting for a logistics building of the Stormcloak Army or the City Guard, than an ordinary residence or a shop.

This building is referred to as the Northeast District Warehouse, as it likely contain supplies for the soldiers. It perhaps could also be a stable for horses, but only if it makes sense for horses to be able to go in and out of those normal rectangular doors.