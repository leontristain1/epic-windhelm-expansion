# Road To Winterhold

NOTE: This work is currently IP and will be released in the future.

![](/images/northeastgate3.png)

The Road to Winterhold is an addon mod for EWE, that will pave a northward road from the [Northeast Gate](northeastgatehouse.md) of Windhelm to connect to the vanilla road to Winterhold. The road follows the path shown in the below image, and should dramatically cut down the travel time between Windhelm and Winterhold, its closest allied city.

![](/images/roadtowinterhold.png)