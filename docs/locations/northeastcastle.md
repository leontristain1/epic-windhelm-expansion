# Northeast Castle

![](/images/northeastdistrictcastle4.png)

Just like the [Western Fort](westernfort.md) anchors the [Southern Wallwalk](southernwallwalk.md) into the mountains, the Northeast Castle serves the same purpose and anchors the [Northeast Wallwalk](northeastwallwalk.md) into the mountains, and is the primary garrison for Windhelm's city guards in the northeast.

The Northeast Castle has many access points. The main gate is located at the wallwalk level, well protected along the the portcullis-integrated cross ways where the wallwalk goes through the castle structure to connect to the [Northeast Castle Back Plaza](northeastcastlebackplaza.md). Opposite of the gate is a narrow walkway to a railing above the [Northeast District Pub](pub.md), that provides views to the south. At the very bottom, the Northeast Castle has a ground-level doorway located close to the [Northeast District Warehouse](northeastdistrictwarehouse.md).

Then high above, the castle has two balconies. The lower balcony is a very large balcony lined with battlements that overlook the area just outside the city walls, and serves the purpose of the castle as a defensive tower. The upper balcony points south, and has stone stairs that ascend up to the castle roof.

Finally, a door to the west of the castle opens up to the [Northern Wallwalk](northernwallwalk.md). The castle serves as the eastern terminus of the Northern Wallwalk, and along the wallwalks one can travel all the way west to get to the [Mountain Caves](mountaincaves.md) via the [Northern District Temple](northerndistricttemple.md).

There is also a [Northeast Castle Side Room](northeastcastlesideroom.md) that is part of the same structure but separate from the castle proper.