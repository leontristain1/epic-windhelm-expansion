# Arena Market Shops

![](/images/arenamarket9.png)

In the lower area of the [Windhelm Arena Market](arenamarket.png), right next to the [Northeast District Temple](northeastdistricttemple.md), there are three shop doors that leads to interiors. These are known as the Arena Market Shops, and are considered part of both the Arena Market and the [Northeast District](northeastdistrict.md).

Two of the shops are built right into the wall between the Northeast District and the Gray Quarters. The third shop is on the opposite side, underneath the stone bridge.

![](/images/arenamarket7.png)