# Western District Residences

![](/images/westerndistrictalleys2.png)

In the [Western District](westerndistrict.md), in addition to the [Meadhall Apartments](meadhallapartments.md), there are 4 other generic doors that can lead into various NPC homes and shops. These are collectively known as the Western District Residences.

Residences 01, 02, and 03 are located right next to the Meadhall Apartments in the same alley. Residences 04 is much further away, located further to the west past [Windswept Manor](windsweptmanor.md), at the foot of the stairway up to the [Beacon of Windhelm](beacon.md).