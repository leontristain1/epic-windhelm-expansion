# Five Taverns of Windhelm

Vanilla Windhelm had two tavern-like places: Candlehearth Hall and New Gnisis Cornerclub. In the expanded windhelm, we added three new tavern-like places. Together, they can be collectively referred to as the Five Taverns of Windhelm.

* [Candlehearth Hall](https://en.uesp.net/wiki/Skyrim:Candlehearth_Hall) (serves Stone Quarter, Northern District)
* [New Gnisis Cornerclub](https://en.uesp.net/wiki/Skyrim:New_Gnisis_Cornerclub) (serves Gray Quarter in the east)
* [Meadhall](meadhall.md) (serves Valunstrad, Western District, Northern District)
* [Windswept Manor Commons](windsweptmanorcommons.md) (serves Western District, Windswept Manor)
* [Northeast District Pub](pub.md) (serves Northeast District)

### Candlehearth Hall
![](/images/candlehearth1.png)

### New Gnisis Cornerclub
![](/images/arenamarket14.png)

### Meadhall
![](/images/meadhall2.png)

### Northeast District Pub
![](/images/pub1.png)

### Windswept Manor Commons
![](/images/windsweptmanorinterior1.png)