# Beacon of Windhelm

![](/images/beacon1.png)

In the western part of Windhelm, perhaps the most visible structure of all is the Beacon of Windhelm. This is a massive lighthouse platform that sits on top of mountain cliffs, visible from miles away. To access it, you would need to travel to the westernmost edge of the city, and then keep on going until you get to the most remote back corner. There, you will find a flight of steps among the secluded trees where you can climb to get to the top.

Prepare yourself, for the stairs are very, very steep, and it goes up very, very high. But if you take the journey, what awaits you at the end is an observation platform that offers you an immensely epic bird's eye view of the city.

At the beacon platform level, there is a door that leads into the [Mountain Caves](mountaincaves.md). If the city is overrun by enemies, defenders may find it necessary to retreat into the caves by way of the [Mountain Gate](mountaingate.md). In such a scenario, the beacon platform becomes a place easily taken and defended by the besieged force from the caves, giving them the advantage of high ground and sight.

The beacon's purpose is to signal to Windhelm's allies in times of danger. When the city is besieged, the guards of Windhelm will change the fuel and have the flame burn an intense red, and allies afar would see it as a distress signal. Hopefully, when the Windhelm Beacon is lit and Windhelm calls for aid, Riften will answer.

When times are peaceful, the beacon will always be lit with a normal-colored flame, and serve as a point of orientation for nearby travelers.