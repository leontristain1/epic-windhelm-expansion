# Eastern Tower

![](/images/easterntower3.png)

The Eastern Tower is a wall tower located in-between the [Gray Quarter Wallwalk](grayquarterwallwalk.md) and the [Northeast Wallwalk](northeastwallwalk.md), nestled in-between the [Northeast District Temple Back](northeastdistricttempleback.md) building and the [Northeast Gatehouse](northeastgatehouse.md).

This tower is rather unique in that it is completely exposed to the outside. There are no associated interior cell. The tower has spiraling stairs along its center where one can ascend and descend. The stairs serve three levels. The lower level has an opening to a flight of wooden stairs that leads to the ground. The upper level has two openings that leads to the wallwalks that it is a part of. Then there is the ceiling level at the very top.

At the wallwalk level, the tower is surrounded by wall-level platforms on the outside, making it so that players and NPCs can choose to walk around the tower instead of walking through it. This improves access overall because the interior of the tower is rather cramped.

![](/images/easterntower2.png)