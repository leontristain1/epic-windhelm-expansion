# Northeast Gatehouse

![](/images/northeastgatehouse1.png)

One of the main goals of EWE is to provide a gate that opens to Windhelm's northeast. In the vanilla game, there is an awkward amount of space in this direction that is not used by any important game content, and yet the only proper road that leads to Winterhold - Windhelm's closest ally - is a windy path that goes far out to the west before going north. A road could clearly be built to the northeast to cut a lot of travel time, and yet it wasn't.

And now it is. With the expanded Windhelm, we now have a gate to the northeast, and an addon mod that properly paves the [Road to Winterhold](roadtowinterhold.md) is close to completion. The gate to the northeast lies in the [Northeast District], and comes with a gatehouse structure known as the Northeast Gatehouse.

From inside the city, the gatehouse's interior is accessed from the ground level through a door located on its right side, next to the wooden stairs that lead up to the [Eastern Tower](easterntower.md). At the wallwalk level, there are additional doors that open right onto the wallwalk. This makes the gatehouse one of the options available for traveling between the ground level and the wallwalk level.

The outer facade of the gatehouse, that looks out into the Winterhold direction, is dominated by two gatehouse half-towers, each with the top accessible via stone stairs directly from the wallwalk.

![](/images/northeastgate2.png)