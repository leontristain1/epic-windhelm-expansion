# Windswept Manor Commons

![](/images/windsweptmanorinterior1.png)

Windswept Manor Commons is the common area on the first floor of [Windswept Manor](windsweptmanor.md). All [Windswept Manor Subdivisions](windsweptmanorsubdivisions.md), including the [Windswept Manor Suite](windsweptmanorsuite.md), are connected to the commons, which then connect to the outside. The inner half of the commons is a tavern-like space where Windswept Manor residents eat and socialize, where food and drinks are served. The food and drinks are prepared in the [Windswept Manor Kitchen](windsweptmanorkitchen.md).