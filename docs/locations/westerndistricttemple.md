# Western District Temple

![](/images/westerndistricttemple1.png)

The Western District Temple is a massive temple sitting next to the [High Road](highroad.md). It is the tallest freestanding building in the western half of Windhelm. The temple has two entrances at the ground level, one in the front, with stairs that descend down to the [Western District](westerndistrict.md)'s alleys, and one in the back, with stairs that ascend up to the [Mountain Gate](mountaingate.md).

There is also a sky bridge that connects the temple's upper levels into the [Mountain Caves](mountaincaves.md). This sky bridge also allows for a third entrance that opens to an elevated passageway at the roof level of the [Western District Residences](westerndistrictresidences.md) around the base of the rocks that prop up the [Beacon of Windhelm](beacon.md).

![](/images/westerndistricttemple2.png)

Folks have suggested what kind of temple this could be. I've heard that it is good for it to be a temple to Kyne/Kynareth, for she is the origin of the Thu'um and the mother of the Nords. Since the death of Shor, Kyne is considered the chief diety of the Nord pantheon. If the temple is to be dedicated to Kyne, it would also make sense to have smaller shrine rooms to Shor and perhaps also Mara, Kyne's handmaiden.

Another idea brought up is that it could be a Temple of the _Nine_ Divines, making it contrast starkly with Solitude's Temple of the Divines, and highlighting the Stormcloak position. This could be a counter-pilgrimage site. The divines could be addressed by old Nordic names. Perhaps Ulfric made this happen in order to forge a new nationalistic identity (or, the equivalent of propaganda, if you spin it negatively).

Note: given that we have so many temples now, we can have them all! Though the most major of temples should probably now go into the [Northern District Temple](northerndistricttemple.md) which now has the most epic structure.