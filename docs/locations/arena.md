# Windhelm Arena

![](/images/arena2.png)

Any Windhelm city expansion mod cannot be complete without addressing the Windhelm Arena, a cut part of vanilla game content. Currently, EWE only restores the _**structures**_ of the Windhelm Arena. This includes two parts.

First, the prison building in-between the Aretino Residence and the Palace of the Kings Courtyard is replaced with an alternative prison building. This building is available in the vanilla game, but not placed anywhere in the game world. In comparison, the alternative building is a bit taller, and has a `+` shaped passage instead of the `L` shaped passage in the normal prison building. The north exit of the `+` shaped passage leads into the Windhelm Arena's courtyard.

Second, the structure of the Windhelm Arena itself is restored to where it would have been, complete with its magnificent gate, which has been linked to the corresponding door in the [`WindhelmPitWorldspace`](https://en.uesp.net/wiki/Skyrim:Test_Cells#WindhelmPitWorldspace). Then, we give it a face lift by wrapping it in some nice wall pieces with a dark finish, complete with arched facades and cage-like spikes at the top, making it look both polished and more distinct compared to the vanilla structure.

Please note that currently the structures are there and navmeshed, however, no attempt at restoring any gameplay content has been done. The plan is to eventually try to patch in an existing mod that restores the arena's gameplay content, such as maybe [Faction - Pit Fighter](https://www.nexusmods.com/skyrimspecialedition/mods/22513).

A Talos statue has been placed next to the Arena, facing the [Windhelm Arena Market](arenamarket.md)

![](/images/arena1.png)