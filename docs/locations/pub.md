# Northeast District Pub

![](/images/pub1.png)

The Northeast District Pub is located at the end of an alleyway to the left as you descend down to the [Northeast District](northeastdistrict.md) from the [Windhelm Arena Market](arenamarket.md). It is a building facade built into the lower portions of the [Northeast Castle](northeastcastle.md).

This pub is the prime pub for the Northeast District, which is quite some walk away from the other four of the [Five Taverns of Windhelm](fivetaverns.md). The crowd here is rather local, and the interior should provide a cozy atmosphere.