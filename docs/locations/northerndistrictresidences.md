# Northern District Residences

![](/images/northerndistrictresidences2.png)

In the [Northern District](northerndistrict.md) there are 2 generic doors that
can lead into various NPC homes and shops. These are collectively known as the Northern District Residences.

Residence 02 is on the other side of Northern District Mansion, while Residence 03 is further down, facing the stairs that go down to the Palace of the Kings. Residence 01 used to be a thing but has been removed in version 0.3 of the mod. The [Northern District Temple](northerndistricttemple.md) changes resulted in there being no more room for it.