- [Overview](#overview)
  - [Summary](#summary)
  - [Three New City Districts and Additional Contents](#three-new-city-districts-and-additional-contents)
  - [Wallwalks and Battlements of Windhelm](#wallwalks-and-battlements-of-windhelm)
  - [Repairs to Vanilla Windhelm Worldspace](#repairs-to-vanilla-windhelm-worldspace)

# Overview

## Summary

**Epic Windhelm Expansion** (EWE) is a Windhelm expansion mod for Skyrim Special Edition. Broadly speaking, **Epic Windhelm Expansion** tries to do the following.

* Expands Windhelm's footprint to the west, north, and northeast directions, resulting in 3 entirely new city districts.
* Restores and makes accessible the wallwalks of Windhelm, turning them into a picturesque and functional road network to be used by player and NPCs.
* Enhances the city walls with epic-looking battlements, towers, gatehouses, and castles, complete with crenelations with machicolations underneath.
* Adds many epic structures and scenic environments to the expanded Windhelm city in a way that boosts the overall level of atmosphere of Windhelm while remain lore-friendly and not overdone.
* Repairs many problems in the vanilla Windhelm worldspace, such as missing or hidden terrains, missing standins between worldspaces for close up cells not covered by LODs, and various other imperfections in vanilla buildings when viewed from nonstandard angles. Overall, the fixes should make the Windhelm worldspace look whole and complete from any vantage point in the newly expanded city.
* Provides a northeastern gate that exits to the flat snowy plains to Windhelm's northeast, allowing a more sensible and straightforward road to Winterhold to be paved.

Overall, this massively expands the playable area of the city of Windhelm, and adds many new locations and spectacles ready for content.

![](/images/overview-vanillacompare.png)
![](/images/overview-structuresanddoors.png)

Before we begin, I should disclaim that each of the newly added location is currently referred with a generic sounding name, like "South Wall Tower", "Mountain Gate", "Northern District Mansion", "Northeast Castle", "Northeast District Pub", or "Eastern Tower". These may be renamed into something more creative when gameplay content is eventually implemented. For now, the document refers to them with these generic names.

Given the "base mod + addons" [Call For Help](helpwanted.md)-scheme currently being attempted, it's totally possible for these structures to not even be what the generic name suggests. For example, the "Windhelm Bathhouse" building space might actually be developed into a library, a school, or anything else the addon author might wish for it to be.

With that out of the way, we will now go in-depth about each aspect.

## Three New City Districts and Additional Contents

![](/images/overview-districtsandareas.png)

The mod begin with the idea of taking vanilla Windhelm's southern walls and extend it all the way west until it touches the mountain. In doing so, we carve out space for 2 new districts - a Western District, and a Northern District

Then, out of desires for a gate to the northeast, a restored Windhelm Arena, and have Gray Quarter remain a lore-friendly cull-de-sac without strategic importance, we further expanded Windhelm into the northeast, resulting in the Northeast District.

To read more about each individual districts and see what they have to offer, please refer to their own individual articles:

* [Western District](locations/westerndistrict.md)
* [Northern District](locations/northerndistrict.md)
* [Northeast District](locations/northeastdistrict.md)

In addition to the 3 new districts, there are other content additions. Many of which are related to the wallwalks and battlements, which will be covered in the next section.

Notably, the vanilla market square has been extended westward by about 50%. Also, the Windhelm Arena structure, which was vanilla content that was cut from the vanilla game, has been restored to where it would've been. For more details about these changes, please refer to their own individual articles:

* [Expanded Market](locations/expandedmarket.md)
* [Windhelm Arena Structural Restoration](locations/arena.md)

## Wallwalks and Battlements of Windhelm

![](/images/overview-wallwalknetwork.png)

In vanilla Windhelm, the tall, majestic city walls are not intended to be accessible. The curious god-player with flying abilities could occasionally fly up to it, and all they would see are a bunch of rough models with bad placements and inaccurate collision. In fact, a lot of the city walls that aren't visible from typical vantage points on the ground, are strictly missing.

The city walls, I think, is a huge lost opportunity in the vanilla game. Thus, EWE restores it all. The wallwalks are now fully accessible, with multiple, sensible-yet-nonintrusive access points where the player can climb up to the walls from the ground level.

In addition, when EWE expanded the city further to the west, it kept the vanilla western walls as a natural partitioning wall between the vanilla quarters and the new Western District. This wall has been further developed into the Highroad - a raised avenue that runs right down the middle of that area of the city.

Likewise, when EWE expanded the city further to the northeast, it kept the vanilla walls north of the Gray Quarter as a natural partioning wall between the Gray Quarter and the new Northeast District. This wall runs right from the exterior walls to the Windhelm Arena, and has been redeveloped into the Windhelm Arena Market - a covered market avenue that serves as one of Windhelm's spectacles.

Overall, the city's exterior walls, the Highroad, the Windhelm Arena Market, and the Northern District which itself sits at a high elevation, together forms a raised "wall-level" road network that is "grade-separated" from the normal walkways on the ground level. This gives the city a multi-layered feel, and helps address the common complaint that Windhelm's layout is "nonsensical" and easy to get lost in. The idea is that whenever you are lost, you can find your way onto the walls, and you'll get a beautiful, high-vantage point view of the city with which you can reorient yourself.

The city's exterior walls have also been outfitted with many [battlements, towers, bastions, and gatehouses,](locations/battlements.md) all of which are lined to the brim with crenelations and machicolations, which hopefully contributes to the epic mood of the city as a realistically fortified stronghold.

To read more about the various wallwalk-related locations, please refer to their own individual articles:

* [Battlements of Windhelm](locations/battlements.md)
* [Southern Wallwalk](locations/southernwallwalk.md)
* [High Road](locations/highroad.md)
* [Dockside](locations/dockside.md)
* [Gray Quarter Wallwalk](locations/grayquarterwallwalk.md)
* [Windhelm Arena Market](locations/arenamarket.md)
* [Northeast Wallwalk](locations/northeastwallwalk.md)
* [Northern Wallwalk](locations/northernwallwalk.md)

## Repairs to Vanilla Windhelm Worldspace

Vanilla Windhelm has a lot of things that were basically broken, such as:

* Lots of landscapes were disabled or missing, in cells too close to be covered by LODs.
* A complete lack of standins for structures outside the city in cells too close to be covered by LODs, such as the Windhelm Bridge and nearby farmhouses.
* Occlusion boxes for certain buildings were set too high, resulting in objects popping in and out of view when you get close to the building's roof from certain angles.
* Lots of vanilla building nifs, mostly in the Gray Quarter, had huge, gaping holes that show obvious empty interiors when viewed from above.

![](/images/vanillafixupsbefore.png)

None of these problems are noticeable if you play a purely vanilla game and view things from purely vanilla angles. However, the moment you start cheating, playing with console commands, installing mods that let you fly, "blink" to places, etc... you'll see and experience them. In the vanilla game, "purely vanilla angles" works because they are extremely limited since the vanilla game wouldn't let you get anywhere close to the wallwalks.

In EWE, you get to access the wallwalks and climb up the towers, which means you can look out into the surrounding landscape in _every_ direction. Therefore, all surrounding landscape cells that are close enough to not be covered by LODs (assuming the typical case of `uGridsToLoad=5`) will have its Windhelm worldspace cell enabled and landscape from the Tamriel worldspace inherited. This results in smooth, seamless landscapes and waters.

Nearby large structures, such as the Windhelm Bridge, also gets standin copies into the Windhelm worldspace. This means when you look out from the Southern Walls, you'll see the enormous Windhelm Bridge, and it'll have you wonder about what it might be like when the Imperial Army is invading.

All the holes in vanilla Windhelm building models, such as numerous Gray Quarter models, have also been patched up by adding various wooden boards and other objects to cover up those holes. When you are on the Gray Quarter Walls, and you look down at the Gray Quarter, you will not see any holes, it'll look like the buildings have proper roofs where there's no reason people wouldn't be able to live in their interiors.

![](/images/vanillafixupsafter.png)
(Note: this image is taken prior to the 0.2.0 "Battlements" update so all later changes are not shown, but it should still illustrate the idea of vanilla problems being fixed up.)

