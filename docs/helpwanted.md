- [Third Party Contributions](#third-party-contributions)
  - [What Are Addons](#what-are-addons)
  - [How Addons Will Be Managed](#how-addons-will-be-managed)
    - [I suggest the following naming convention](#i-suggest-the-following-naming-convention)
    - [My work on the base mod will aim to make creating addons easier](#my-work-on-the-base-mod-will-aim-to-make-creating-addons-easier)
    - [I will work on a door patcher to handle the only unavoidable coupling between the base mod and addons](#i-will-work-on-a-door-patcher-to-handle-the-only-unavoidable-coupling-between-the-base-mod-and-addons)
    - [I would love it if you let me know you're working on an addon](#i-would-love-it-if-you-let-me-know-youre-working-on-an-addon)
    - [I would recommend modularity](#i-would-recommend-modularity)
    - [I would recommend open permissions for inclusion in merged content packs](#i-would-recommend-open-permissions-for-inclusion-in-merged-content-packs)
    - [If you require base mod changes](#if-you-require-base-mod-changes)

# Third Party Contributions

As I've grown to realize, EWE is an enormous project, and I have limited time to work on this mod. If nothing else happens, I will likely chip away at this mod slowly over time. Given my current rate of progress, it will probably take until early 2022 for a first incomplete version with enough NPCs and interiors to be deemed "playable", and probably take until late 2022 for a version that might be considered "complete".

If you are a mod author and want to see this project happen faster, and feel like contributing somehow, I would welcome the help. There are two ways we can get this done.

* You can fork the base mod and do what you like
* You can help create an addon for the base mod

First and foremost, permissions are fully open for anyone to fork this mod at any point in time, for as long as 3rd party assets used have their transitive permissions followed. All 3rd party assets I've used have open permissions, so you would not need to re-obtain permissions. I would also appreciate a credit somewhere, but if you forget I most likely won't notice, and certainly won't go after you. So at the very basic, forking this mod, and taking it to whatever directly you like, is always an option.

Next, my plan for potential 3rd party contributions look like the following:

1. I will build out `EpicWindhelmBase.esp` as a base platform upon which anyone can build content addons, and encourage folks to contribute addons that are standalone and modular.
2. I will create my own modular content addons to flesh out the various interiors, NPCs, quests, decor, etc... for the mod. These content addons can be used together with other addons that don't conflict, or simply serve as alternatives to other addons that do conflict.
3. I will eventually merge a set of content addons, including my own, and with permission if including others, into a content pack, which together with the base mod, can be considered a completed version of this mod.

The reason I'm doing it this way instead of trying to put together a team and then managing a team project, is because I don't have the cycles to manage a team project. Done this way, 3rd party contributors can have full creative freedom and independence without need for coordination, and the future is kept more flexible and low-stake.

## What Are Addons

Addons are mods that depend on `EpicWindhelmBase.esp`, that seeks to modularly build out interiors, add NPCs, and add gameplay content such as merchants, interactions, and quests.

Addons can do whatever they want within interior cells for as long as existing interior doors are _**reused**_, and their door links to the exterior doors are _**never broken**_. The interior doors can be moved around, relocated to different interior cells, or finalized onto different interior navmesh triangles, without issue. Addon authors can choose to lock or unlock the interior door.

Addons can perform _additive_ changes to the Windhelm worldspace, including adding clutter and decor, and dropping various behavioral markers for NPCs to use.

Addons are not allowed to touch exterior navmesh in the Windhelm worldspace. However they can add `L_NAVCUT` boxes additively to control navmesh.

The only records in the Windhelm worldspace addons are allowed to override, are the exterior door records, where the teleport destination override is unavoidable. I will write a door patcher xEdit script at some point as a utility to make managing any such door conflicts easy.

## How Addons Will Be Managed

### I suggest the following naming convention

* The base mod will be called `EpicWindhelmBase.esp`
* Addons are encouraged to have a plugin name like `EpicWindhelm-{ModName}.esp`
  * Example: `EpicWindhelm-ltrTempleofKyne.esp`
  * Example: `EpicWindhelm-NarrativeLoot-Patch.esp`

### My work on the base mod will aim to make creating addons easier

In the base mod, I will gradually add features that will make addon contribution easier. For example, in the next 0.4 release, I hope to shell out the interior cells and add idle markers across the expanded Windhelm exterior.

The hope is that an addon author, if they wanted, could do as little as simply decorate the interior of a few houses, plop down a few NPCs, add some daily schedules, and call it a day.

On the other hand, any work done in the interior cells in the base mod will be limited to architectural layout only, such that if someone wants to tear something down and rebuild entirely from scratch, it would be fairly easy to disable the existing stuff.

### I will work on a door patcher to handle the only unavoidable coupling between the base mod and addons

From what I can see, there is one unavoidable coupling between the base mod and addons. I will demonstrate with an example.

Suppose an addon wants to build out and interior cell and put an NPC there. The addon would otherwise not touch the exterior Windhelm worldspace except in purely additive ways, such as adding a shop sign outside the building, or adding various behavioral markers for the NPC to use. Here, the desire is to maintain separation of concerns between the addon and the base mod, keeping the two decoupled and able to change independently from each other.

The one unavoidable coupling happens at the door records. There is an exterior and an interior door, linked to each other. For both door records:

* The plugin responsible for the cell the door is in has an opinion on its XYZ coordinates and navmesh triangle.
* The plugin responsible for the cell the door is linked to, has an opinion on its teleport destination.

Since the addon will win the conflict on both doors between the base mod and the addon, it creates a situation where here, if the base mod ever updates the door's position or its navmesh triangle, the addon would revert the change if the addon was built from an older version of the base mod. In the scenario where the navmesh triangle have been updated, it would further result in a mismatch between the exterior door's navmesh triangle, and the exterior navmesh's door triangle, which is even worse.

In the absence of a way to resolve this conflict, the addon must be kept up to date with each base mod update that changed its exterior door. To better help manage this conflict, I will at some point write an xEdit script that automatically merges conflicts between base mod and addons for these exterior doors. The script would make sure the exterior door has the addon's opinion on teleport destination, and the interior door has the base mod's opinion on teleport destination, but keep all other data to the base mod or addon, respectively.

### I would love it if you let me know you're working on an addon

If you want to work on an addon, please consider reaching out to me on NexusMods so we can keep each other casually informed.

### I would recommend modularity

Addon authors are encouraged to break up their mod into sensible modular pieces. Here are some examples of what a modular break up can look like.

> Suppose I want to build out the Bakery's interior, and add two NPCs, Sven and Hilda, to live there. I also want a helper called Siegfrid who work there during the day but lives at one of the Meadhall's apartments.
> 
> I can do all this in a single addon. Or I can split it into two addons. One would contain only the Bakery content with Sven and Hilda. The other would contain only Siegfrid and his apartment content, and depend on the first addon to send Siegfrid to the Bakery during the day.

Here is another example:

> Suppose I want to build out the Northern District Temple. To do this, I want to create the elaborate interior with NPCs, but I also want to add clutter and decor to the area in front of it, so it looks better from the outside.
>
> I can do this in a single addon, or I can do split it into two addons. One would contain the interior buildout of the temple, complete with NPCs. The other would become a standalone exterior clutter/decor addon.

Modularity is encouraged because it offers two advantages:

* Users can choose more granularly among conflicting addons.
* The resulting plugins could each be smaller than the combined plugin, increasing the chances of them being _ESL-ified ESPs_. The main disadvantage of addons is the potential bloating of the user's load order. Given that, the more _ESL-ified ESPs_ we have, the better we are.

But of course, whether breaking up an addon is a good decision is really a judgment call depending on the mod. All I ask is to at least consider modularity.

### I would recommend open permissions for inclusion in merged content packs

Addon authors are encouraged (but of course not required) to provide open permissions for the addon to be merged, by myself or others, into merged content packs. The main disadvantage of addons is the potential bloating of the user's load order. If many addons cannot be _ESL-ified ESPs_, then merged content packs are the other way this disadvantage can be addressed.

I will eventually try to create such a pack. All my content addons will have completely open permissions to be merged into content packs.

### If you require base mod changes

If you're working on the addon and you feel strongly about something in the Windhelm worldspace that you want changed (where purely additive changes from your side are insufficient to make it happen), then let's chat on NexusMods. Unless I have my own very strong reasons against it, I can most likely make the change in the next release, or at least put it onto my list of future work to get to. If you are blocked by this issue, then I will try to raise its priority on my side.

Note that I do reserve the right to disagree about the content and to prioritize it as I see fit. In this case, if you can't wait for me, or if you absolutely want a version of the base mod with the changes made, then there is always the option of forking my base mod, since my permissions are completely open.
